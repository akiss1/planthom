include "getARGV.idp"
load "iovtk"
/*************************************************************
 *                  PARAMETERS
 * **********************************************************/
 
int gt = getARGV("-gt", 1);  // growth type

int anew = getARGV("-newmesh", 0);

// time variables
int Ntpoints = getARGV("-nt", 5);

real tmax = getARGV("-tmax", 1.0);
real t=0.0, dt;
dt=tmax/(Ntpoints-1);

cout<<"time integration parameters:"<<endl;
cout<<"tmax="<<tmax<<"   nt="<<Ntpoints<<"   dt="<<dt<<endl;


string name="MicroscopicSimulation_gtype"+string(gt);


// mechanical parameters
real Ecw = getARGV("-Ecw", 1.0);  // Ecw
real nu=getARGV("-nu", 0.3);  // nu
real Pressure0=getARGV("-P0", .001);
real sigmaThreshold=getARGV("-sigmaThreshold", 0.);
real sigmaC=getARGV("-sigmaC", 1.);

// ---------------- geometrical parameters
int d=2;

// cell dimensions
real l1 = getARGV("-l1", 1.);
real l2 = getARGV("-l2", 1.);
real theta=getARGV("-theta", 30)*pi/180;

real L = getARGV("-L", 1.);
real H = getARGV("-H", 1.);

real hwall = getARGV("-wall", 0.1);
real hextwall = getARGV("-extwall", 0.1);

real ml=0.1;  // middle lamella 1/2 thickness
real cw=hwall-ml;  // cellwall thickness

// tissue dimensions
int Nx=getARGV("-Nx", 3);
int Ny=getARGV("-Ny", 3);

// mesh parameters
int nvertex=getARGV("-nvertex",20);
cout << "nvertex="<<nvertex<<endl;

// mesh parameter for the macroscopic mesh
int nxmacro=getARGV("-nxmacro", 25);

string coupleddirname="CoupledSimulation_wall"+string(hwall/l1)+"_gtype"+string(gt)+"/Macroresults/";

string dirname=name+"_wall"+string(hwall/l1)+"_theta"+string(theta*180/pi)+"_Nx"+string(Nx)+"_Ny"+string(Ny)+"_P"+string(Pressure0)+"_nx"+string(nvertex)+"_nt"+string(Ntpoints)+"_tmax"+string(tmax);
string root=dirname+"/";
exec("mkdir " + dirname);
exec("cp parameters.py " + dirname);

string rootdef=dirname+"/deformed/";
exec("mkdir " + rootdef);

string rootdefpng=dirname+"/deformed-png/";
exec("mkdir " + rootdefpng);

string rootgrowth=dirname+"/growth/";
exec("mkdir " + rootgrowth);

ofstream uLHfile(root+"microscopic_P"+string(Pressure0)+"_wall"+string(hwall)+"_LH.csv");
ofstream Fgfile(root+"Fg_microscopic_P"+string(Pressure0)+"_wall"+string(hwall)+".csv");
ofstream errorfile(root+"error_P"+string(Pressure0)+"_wall"+string(hwall)+".csv");


/*************************************************************
 *                  GEOMETRY and MESH
 * **********************************************************/
include "geometry/geometry_microscopic_Rectangular.cpp";
include "definitions/bib_fe_microscopic.cpp";
include "definitions/bib_general.cpp";
include "definitions/bib_tensors.cpp";
include "visualisation/colormaps.cpp";
include "visualisation/boundingboxes_cell.cpp";
include "visualisation/boundingboxes_tissue.cpp";

real[int] cmp=HSVviridis;


plot(ThCell, ps=root+"cell_geometry.eps", bb=bbcell, wait=false);
plot(ThTissue, ps=root+"tissue_geometry.eps", bb=bbtissue, wait=false);


// definition of integer functions for the structural domains
// ==========================================================

/*
// 1. The walls
Sh0 wallsh=0;
cout<<"Computing middle-lamella and wall regions"<<endl;


for (i=0; i<=Nx*Ny-1; i++)
{
cout<<"C("<<CX[i]<<","<<CY[i]<<")"<<endl;
func wall=int(wallBool(x,y,i));
wallsh=wallsh+wall;
}

plot(wallsh,wait=1,value=true,fill=1, ps=root+"walls.png",bb=bbtissue);

*/



/*
// 3. Distance map
Sh0 disth=0;
cout<<"Computing the distance map"<<endl;
for (i=0; i<=Nx*Ny-1; i++)
{
cout<<"C("<<CX[i]<<","<<CY[i]<<")"<<endl;
disth=disth+DistmapCell(x,y,i);
}

plot(disth,wait=1,value=true,fill=1, ps=root+"distmap.png",bb=bbtissue);
*/

// ---------- parameters ----------

int regionNo;
real averagePerCell, stdPerCell, TrGrowthPerCell;
real Yw;
real XX,YY;


// elasticity tensor
// -----------------
include "definitions/param_Fields_elasticity.cpp";

Sh0 Ecwh=0., Ecellwallh ;
Ecellwallh=Efunc;

Sh0 nuh=0., nucellwallh ;
nucellwallh=nufunc;

// pressure
// --------
include "definitions/param_Fields_pressure.cpp";

Sh0 Ph=0., Pressureh;
Pressureh=Pressure;


// --- extensibility, stress threshold
// ----------------------------
include "definitions/param_Fields_growth.cpp";

Sh0 sigmaCh;
sigmaCh=Cfunc;
plot(sigmaCh, cmm="sigmaC", value=true,fill=1, ps=root+"/sigmaC.eps",bb=bbtissue, wait=false, hsv=cmp);


Sh0 sigmaThresholdh;
sigmaThresholdh=Tfunc;
plot(sigmaThresholdh, cmm="sigmaThreshold", value=true,fill=1, ps=root+"/sigmaThreshold.eps",bb=bbtissue, wait=false, hsv=cmp);



// compute pressure (average) per cell
for (j=0; j<=Ny-1; j++)
{
	 for (i=0; i<=(Nx-1); i++){
		regionNo=cn(i,j);
		Yw=int2d(ThTissue,regionNo)(1);
		averagePerCell = int2d(ThTissue,regionNo)(Ecellwallh)/Yw;
		Ecwh=Ecwh+averagePerCell*(region==regionNo);
		averagePerCell = int2d(ThTissue,regionNo)(nucellwallh)/Yw;
		nuh=nuh+averagePerCell*(region==regionNo);
		averagePerCell = int2d(ThTissue,regionNo)(Pressureh)/Yw;
		Ph=Ph+averagePerCell*(region==regionNo);
}
}


plot(Ecwh, cmm="E_cellwall", value=true,fill=1, ps=root+"Ecw.eps",bb=bbtissue, wait=false, hsv=cmp);
plot(Ecwh, cmm="nu_cellwall", value=true,fill=1, ps=root+"nu.eps",bb=bbtissue, wait=false, hsv=cmp);
plot(Ph, value=true,fill=1, ps=root+"pressure.eps",bb=bbtissue, wait=false, cmm="P", hsv=cmp);




// define the elastic tensor E
Sh0 mu=Ecwh/(1+nuh)/2, lambda=Ecwh*nuh/(1+nuh)/(1-nuh);
E=Etensor(mu, lambda);

macro E0aux [1., 1., 0, 1./2., 0, 0] //



// Egt = id for strainbased growth, =E for stressbased growth
cout<<"---------------------------------"<<endl;
cout<<"gt="<<string(gt)<<endl;
if (gt==0) {
	Egt=E0aux;
	cout<<"Strain based growth hypothesis"<<endl;
	}
else {
	cout<<"Stress based growth hypothesis"<<endl;
	Egt=E;
	}
cout<<"---------------------------------"<<endl;



// external force
macro f [f1, f2] //
Vh2 f;
f=[.2, 0];

// growth
Fg=id;

uLHfile<<"t"<<";"<<"L"<<";"<<"H"<<";"<<"u1(L,H)"<<";"<<"u2(L,H)"<<";"<<"sig0(L,H)"<<";"<<"sig1(L,H)"<<";"<<"sig3(L,H)"<<";"<<"G2hom0"<<";"<<"G2hom1"<<";"<<"G2hom3"<<endl;
Fgfile<<"t"<<";"<<"i"<<";"<<"j"<<";"<<"cell"<<";"<<"area"<<";"<<"Fg0mean"<<";"<<"Fg0std"<<";"<<"X"<<";"<<"Y"<<";"<<"TrGrowth"<<endl;


errorfile<<"nu"<<";"<<"Pressure0"<<";"<<"L"<<";"<<"H"<<";"<<"Nx"<<";"<<"Ny"<<";"<<"t"
<<";"<<"RelMeanErroru1"<<";"<<"RelMeanErroru2"<<";"<<"RelMeanErrorG2"
<<";"<<"meanGrowthCoupled"<<";"<<"meanGrowthMicro"
<<endl;



real wallvolume=int2d(ThTissue)(1);



// ================= time evolution ============================
for(t=0; t<=tmax; t+=dt){  
// ================= time evolution ============================

	
cout<<"t="<<t<<endl;

// Elastic equilibrium after pressure + external force applied

/*
problem eleq(u,v) =
			int2d(ThTissue)( elasticity1(E,u,Fg,v)  )          
			+ int2d(ThTissue)( elasticity0(E,Fg,v)  )
			+ int1d(ThTissue,1000)( pressureonboundary(Ph, Fg,v) )
			+ on(leftwalls, u1=0) //
			+ on(bottomwalls, u2=0) // 
			;

eleq;
*/


varf va(u,v) = int2d(ThTissue)( elasticity1(E,u,Fg,v)  ) // bilinear form
//			+ on(leftwalls, u1=0) //
			+ on(bottomwalls, u2=0) // 
			; 

varf vL(u,v) = 	- int2d(ThTissue)( elasticity0(E,Fg,v)  )   // linear form    
			- int1d(ThTissue,1000)( pressureonboundary(Ph, Fg,v) )
			;

varf vb(u,v) = int2d(ThTissue)(1.*v1); // linear form for introducing the constraint int(u1)=0 by Lagrange multiplier


// build the block matrix for the lhs
int n = Vh2.ndof;
cout << "n="<<n<<endl;

matrix A=va(Vh2,Vh2);

real[int] b(n);
b = vL(0,Vh2);

real[int] sol(n);


// now take into account the lagrange multiplier 
real[int]  B = vb(0,Vh2);

matrix AA = [ [ A ,  B ] ,
              [ B', 0 ] ] ;

// build the block matrix for the rhs
real[int]  bb(n+1),xx(n+1),b1(1),l(1);

bb = [ b, 0];

// solve the linear system
set(AA,solver=sparsesolver);
xx = AA^-1*bb; 

cout<<"LM problem solved"<<endl;

[b,l] = xx;  // set the value 
cout << " l = " << l(0) <<  " ,  b(u,1)  =" << B'*b  << endl; 


u1[]=b; // causes the copy of u2 also (!!!!) 
//(see page 72 of http://www3.freefem.org/ff++/ftp/freefem++doc.pdf)

real translx=u1(0,0);
real transly=u2(0,0);

//u1=u1-translx;
//u2=u2-transly;

mesh Thm1=movemesh(ThTissue, [x+rho*(u[0]-translx), y+rho*(u[1]-transly)]);

//plot(ThTissue, Thm1,  ps=rootdef+"geometry-deformed-superposed_t"+string(t)+".eps",  bb=bbtissue, wait=false, hsv=cmp);
//plot(ThTissue, Thm1,  ps=rootdefpng+"geometry-deformed-superposed_t"+string(t)+".jpg",  bb=bbtissue, wait=false, hsv=cmp);

plot(Thm1,  ps=rootdef+"geometry-deformed_t"+string(t)+".eps", bb=bbtissue, wait=false, hsv=cmp);




// in order the new growth step
// compute average stress (strain) per cell
Sh0 sig0cell=0., sig1cell=0., sig3cell=0., thetah=0.;


// ================ sigma0
thetah=sigma(Egt,u,Fg)[0];

//plot(thetah, cmm="sig0", value=true, fill=1, ps=rootgrowth+"sig0_t"+string(t)+".eps",bb=bbzoom, wait=false, hsv=cmp);
for (j=0; j<=Ny-1; j++)
{
	 for (i=0; i<=(Nx-1); i++){
		regionNo=cn(i,j);
		Yw=int2d(ThTissue,regionNo)(1);
		cout<<"("<<i<<","<<j<<")="<<regionNo<<" : "<<Yw<<endl;
			averagePerCell = int2d(ThTissue,regionNo)(thetah)/Yw; sig0cell=sig0cell+averagePerCell*(region==regionNo);
}}


// ================ sigma1
thetah=sigma(Egt,u,Fg)[1];

//plot(thetah, cmm="sig1", value=true, fill=1, ps=rootgrowth+"sig1_t"+string(t)+".eps",bb=bbzoom, wait=false, hsv=cmp);
for (j=0; j<=Ny-1; j++)
{
	 for (i=0; i<=(Nx-1); i++){
		regionNo=cn(i,j);
		Yw=int2d(ThTissue,regionNo)(1);
		cout<<"("<<i<<","<<j<<")="<<regionNo<<" : "<<Yw<<endl;
			averagePerCell = int2d(ThTissue,regionNo)(thetah)/Yw; sig1cell=sig1cell+averagePerCell*(region==regionNo);
}}


// ================ sigma3
thetah=sigma(Egt,u,Fg)[3];

//plot(thetah, cmm="sig3", value=true, fill=1, ps=rootgrowth+"sig3_t"+string(t)+".eps",bb=bbzoom, wait=false, hsv=cmp);
for (j=0; j<=Ny-1; j++)
{
	 for (i=0; i<=(Nx-1); i++){
		regionNo=cn(i,j);
		Yw=int2d(ThTissue,regionNo)(1);
		cout<<"("<<i<<","<<j<<")="<<regionNo<<" : "<<Yw<<endl;
			averagePerCell = int2d(ThTissue,regionNo)(thetah)/Yw; sig3cell=sig3cell+averagePerCell*(region==regionNo);
}}



G2=[sig0cell, sig1cell, sig1cell, sig3cell];

/*
plot(sig0cell, cmm="sig0cell", value=true, fill=1, ps=rootgrowth+"sig0cell_t"+string(t)+".eps",bb=bbzoom, wait=false, hsv=cmp);
plot(sig1cell, cmm="sig1cell", value=true, fill=1, ps=rootgrowth+"sig1cell_t"+string(t)+".eps",bb=bbzoom, wait=false, hsv=cmp);
plot(sig3cell, cmm="sig3cell", value=true, fill=1, ps=rootgrowth+"sig3cell_t"+string(t)+".eps",bb=bbzoom, wait=false, hsv=cmp);
*/


// diagonalise the stress
// ======================

Sh0 TrG=trace(G2) ;
Sh0 DetG=det(G2);

Sh0 lambdaG1=(TrG+sqrt(abs(TrG*TrG-4*DetG)))/2.;
Sh0 lambdaG2=(TrG-sqrt(abs(TrG*TrG-4*DetG)))/2.;


thetah=-atan2((G2[0]-lambdaG1),(G2[1]));


Sh0 growth1=lambdaG1-sigmaThresholdh; growth1=sigmaCh*absplus(growth1);
Sh0 growth2=lambdaG2-sigmaThresholdh; growth2=sigmaCh*absplus(growth2);


G2=Rtensor([growth1,0,0,growth2],thetah);


thetah=thetah*180/pi;
//plot(thetah, value=true, fill=1, ps=rootgrowth+"thetah_t"+string(t)+".eps",bb=bbtissue, wait=false, hsv=cmp);

// compute growth anisotropy (overwrite on thetah in order to save memory)
Sh0 growthani=(growth1-growth2)/(growth1+growth2+1.e-13);
//plot(growthani, cmm="growth anisotropy", value=true, fill=1, ps=rootgrowth+"grothani_t"+string(t)+".eps",bb=bbtissue, wait=false, hsv=cmp);

// compute trace(growth) (overwrite on thetah in order to save memory)
Sh0 tracegrowth=(growth1+growth2);
//plot(tracegrowth, cmm="trace(growth)", value=true, fill=1, ps=rootgrowth+"TraceGrowth_t"+string(t)+".eps",bb=bbtissue, wait=false, hsv=cmp);


//int[int] Order = [1, 1, 1, 1, 1];
//string DataName = "TraceGrowth Growth1 Growth2 GrowthAni GrowthDir";
//savevtk(rootgrowth+"micro_Growth_t"+string(t)+".vtk", ThTissue, tracegrowth, growth1, growth2, growthani, thetah, dataname=DataName, order=Order);

fesave(tracegrowth, rootgrowth+"micro_TraceGrowth_t"+string(t)+".txt");

real meanTrGrowthMicro=int2d(ThTissue)(tracegrowth)/int2d(ThTissue)(1.);


//plot(G2[0], value=true, fill=1, ps=root+"G20_t"+string(t)+".eps",bb=bbtissue, wait=false);
//plot(G2[1], value=true, fill=1, ps=root+"G21_t"+string(t)+".eps",bb=bbtissue, wait=false);
//plot(G2[3], value=true, fill=1, ps=root+"G23_t"+string(t)+".eps",bb=bbtissue, wait=false);


// Compute the new growth tensor (Euler method)
// ============================================
Fg = summat(Fg,dt*matprod(G2,Fg));


// compute Fg0 average and std per cell


for (j=0; j<=Ny-1; j++)
{
	 for (i=0; i<=(Nx-1)-j/2; i++){
		regionNo=cn(i,j);
		XX=CX(regionNo-1); // coordinates of cell center
		YY=CY(regionNo-1);
		Yw=int2d(ThTissue,regionNo)(1); // area
		averagePerCell = int2d(ThTissue,regionNo)(Fg[0])/Yw;
		sig0cell=sqrt((Fg[0]-averagePerCell)*(Fg[0]-averagePerCell));
		stdPerCell = int2d(ThTissue,regionNo)(sig0cell)/Yw;
		cout<<"("<<i<<","<<j<<")="<<regionNo<<" : "<<Yw<<endl;
		TrGrowthPerCell = int2d(ThTissue,regionNo)(tracegrowth)/Yw;		
Fgfile<<string(t)<<";"<<i<<";"<<j<<";"<<regionNo<<";"<<Yw<<";"<<averagePerCell<<";"<<stdPerCell<<";"<<XX<<";"<<YY<<";"<<TrGrowthPerCell<<endl;
			
}}

/*
// load mesh and data for the coupled simulation
// in order to compare it with the results os the microscopic simulation
// ---------------------------------------------

macro relmeanerr(a,A) sqrt(int2d(ThTissue)(pow(a-A,2)))/sqrt(int2d(ThTissue)(pow(A,2))) //
macro mean(A) int2d(ThTissue)(A)/int2d(ThTissue)(1)//

// construct macroscopic mesh and finite element space
int[int] labs0=[10,20,30,40]; // bottom, right, top, left
mesh ThCoupled=square(L*nxmacro,H*nxmacro, label=labs0, region=0, [x*L,y*H]);
fespace Sh0Coupled(ThCoupled,P1);
fespace VhCoupled(ThCoupled,P2);

// trace growth
Sh0Coupled TrGrowthCoupled;
feload(TrGrowthCoupled, coupleddirname+"coupled_TraceGrowth_t"+string(t)+".txt");
plot(TrGrowthCoupled, tracegrowth, fill=1, value=1, ps=rootgrowth+"TraceGrowth-CoupledSuperposed_t"+string(t)+".eps", bb=bbtissue, hsv=cmp);

Sh0 GrowthCoupled=TrGrowthCoupled;
plot(GrowthCoupled, wait=0, fill=1, value=1, ps=rootgrowth+"TraceGrowth-CoupledProjected_t"+string(t)+".eps", bb=bbtissue, hsv=cmp);


// u0 macroscopic displacement field
Sh0Coupled u0Coupled1, u0Coupled2;
feload(u0Coupled1, coupleddirname+"u1_t"+string(t)+".txt");
feload(u0Coupled2, coupleddirname+"u2_t"+string(t)+".txt");

Sh0 u01=u0Coupled1, u02=u0Coupled2;
Sh0 umicro1=u1-translx, umicro2=u2-transly;

plot(u0Coupled1, umicro1, fill=1, value=1, ps=rootgrowth+"u1_Superposed_t"+string(t)+".eps", bb=bbtissue, hsv=cmp);
plot(u0Coupled2, umicro2, fill=1, value=1, ps=rootgrowth+"u2_Superposed_t"+string(t)+".eps", bb=bbtissue, hsv=cmp);

plot(u0Coupled1, umicro1, fill=1, value=1, ps=rootgrowth+"u1_Superposed_t"+string(t)+"_zoom.eps", bb=bbzoom, hsv=cmp);
plot(u0Coupled2, umicro2, fill=1, value=1, ps=rootgrowth+"u2_Superposed_t"+string(t)+"_zoom.eps", bb=bbzoom, hsv=cmp);


real lambda1=l2*cos(theta);
real delta=lambda1/L;

cout<<"=========================="<<endl;
cout<<"delta="<<delta<<endl;
cout<<"=========================="<<endl;


real RelMeanErrorG2=relmeanerr(tracegrowth,GrowthCoupled);
real RelMeanErroru1=relmeanerr(umicro1,u01);
real RelMeanErroru2=relmeanerr(umicro2,u02);

real meanGrowthCoupled=mean(GrowthCoupled);
real meanGrowthMicro=mean(tracegrowth);

// relative difference map
Sh0 diff=tracegrowth-GrowthCoupled;
real absGrowthCoupled=sqrt(int2d(ThTissue)(pow(GrowthCoupled,2)));
diff=diff/absGrowthCoupled;
plot(diff, wait=0, fill=1, value=1, ps=rootgrowth+"TraceGrowth-RelDifference_t"+string(t)+".eps",bb=bbtissue, hsv=cmp);


cout<<"RelMeanErrorG2="<<RelMeanErrorG2<<endl;
cout<<"RelMeanErroru1="<<RelMeanErroru1<<endl;
cout<<"RelMeanErroru2="<<RelMeanErroru2<<endl;


errorfile<<nu<<";"<<Pressure0<<";"<<L<<";"<<H<<";"<<Nx<<";"<<Ny<<";"<<t
<<";"<<RelMeanErroru1<<";"<<RelMeanErroru2<<";"<<RelMeanErrorG2
<<";"<<meanGrowthCoupled<<";"<<meanGrowthMicro
<<endl;

*/


// ================= time step finished ============================
}
// ================= time step finished ============================


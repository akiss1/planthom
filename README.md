<!----------------------------------------------------------------------------------->
<!--- This file is part of PlantHom.                                              --->
<!---                                                                             --->
<!--- PlantHom is a free software: you can redistribute it and/or modify          --->
<!--- it under the terms of the GNU Lesser General Public License as published by --->
<!--- the Free Software Foundation, either version 3 of the License, or           --->
<!--- (at your option) any later version.                                         --->
<!--- You should have received a copy of the GNU Lesser General Public License    --->
<!--- along with PlantHom.  If not, see <http://www.gnu.org/licenses/>.           --->
<!---                                                                             --->
<!---                                                                             --->
<!--- If you use PlantHom, please cite the following publication :                --->
<!--- DOI:                                                                        --->
<!----------------------------------------------------------------------------------->

# PlantHom : multiscale modelling of growing plant tissues

This is a repository of the simulation script package **PlantHom**, used in the publication
[_Arezki Boudaoud, Annamaria Kiss, Mariya Ptashnyk
**Multiscale Modelling and Analysis of Growth of Plant Tissues**_. SIAM Journal on Applied Mathematics, 2023, 83 (6), pp.2354-2389. ⟨10.1137/23M1553315⟩. ⟨hal-04334494⟩](https://hal.science/hal-04334494)
.

Detailed theoretical as well as implementational considerations can be found in the manuscript.

We are interested in mechanical models of growing plant tissues, where microscopic cellular structure or even subcellular structure is taken into account. In order to establish links between microscopic and macroscopic tissue properties, we perform a multiscale analysis of a model of growing plant tissue with subcellular resolution, and use homogenization to rigorously deduce the corresponding tissue scale continuous model.

**PlantHom** is a script package that contains tools to perform both simulation types: the cellular microscopic simulation, as well as the continuous coupled simulation. For both types of simulations the problems are solved using finite element method implemented in [FreeFEM](https://freefem.org), while the problemsolving processes are orchestrated by [Python](https://python.org) scripts.

## Dependencies and download

**Planthom** depends on [FreeFEM](https://freefem.org) (tested on FreeFem++ 4.1) and [Python](https://python.org) (tested on Python 3.9.7).

You can download it for instance using the `git` command line tool. For that open a terminal window and navigate to the directory of your choice using the `cd` command, then copy the source repository using `git` by typing:
```
git clone https://gitlab.inria.fr/akiss1/planthom.git
```
The script package will be downloaded in a directory called _planthom_.
No compilation needed. Python and FreeFEM scripts are interpreted and executed directly.

## Usage

Open a terminal and navigate in the _planthom_ directory.
Homogeneous parameters are specified in the _parameters.py_ file using Python language.
Both the microscopic and the coupled simulations use this same file for reading the input parameters.

### The unit cell problem (figure 1)

It is the unit cell problem that stays at the basis of the homogenization procedure, it allows to compute the effective, tissue level material properties taking into account cellular geometries and cellular level material properties. The command
```bash
FreeFem++ unitCell.cpp -help 1
```
lists the options to use for model parameters as well as their default values:
```
Usage:
FreeFem++ unitCell.cpp [PARAMETERS]

Model parameters:
   --- Geometry ---
-l1 (default: 1)
-l2 (default: 1)
-theta (default: 30)
-wall (default: 0.05)
   --- Elasticity ---
-Ecw (default: 2)
-nu (default: 0.3)
   --- Growth tensor Fg ---
-Fg0 (default: 1)
-Fg1 (default: 0)
-Fg2 (default: 0)
-Fg3 (default: 1)
```
In order to lounch one single unit cell problem with the default parameters type
```bash
FreeFem++ unitCell.cpp -alone 1
```
The effective material properties $`E_{hom}`$ and $`K_{hom}`$ are computed by homogenization, saved in file _UC0.csv_ and shown on the screen. Snapshots of the original unit cell geometry as well as of those deformed by the elementary deformations $`w^{11}`$, $`w^{22}`$, $`w^{12}`$ and $`v`$ are saved in the current directory.

![](figures/unitcell.png)

In order to lounch a unit cell problem with wall thickness 0.10 and subject to doubling growth along the Ox axis while keeping all other parameters the default value type
```bash
FreeFem++ unitCell.cpp -alone 1 -wall 0.1 -Fg0 2
```

_unitCell.cpp_ is the script used to generate data for figure 1 of the manuscript.

### Growth of a homogeneous plant tissue (figure 2)

In figure 2 of the manuscript we validate the coupled simulation method we developed based on our multiscale analysis. The validation is based on the comparison of the output of the coupled simulation to the output of the corresponding detailed, microscopic model of the whole tissue. Therefore we simulate the growing plant tissue using a microscopic cellular approach on the one hand and the coupled simulation on the other hand, where the tissue is considered a continuum having effective properties that takes into account the cellular geometry and material properties computed by local unit cell problems. The executable python scripts _microscopic\_simulation.py_ and _coupled\_simulation.py_ are used to perform these two types of simulation. 

In order to lounch the microscopic simulation type
```bash
python microscopic_simulation.py
```
A directory named _MicroscopicSimulation_ is created with the results, and for instance a snapshot of the deformed and grown tissue structure at each timestep is saved in the directory _MicroscopicSimulation/deformed_. Here is the first and last snapshot if the simulation is performed with the default parameters.

![](figures/microscopic.png)

In order to lounch the coupled simulation, execute the _coupled\_simulation.py_ executable python script:
```bash
python coupled_simulation.py
```
A directory named _CoupledSimulation_ is created with the results, and for instance a snapshot of the deformed and grown tissue structure at each timestep is saved in the directory _MicroscopicSimulation/Macroresults_. Here is the first and last snapshot if the simulation is performed with the default parameters.

![](figures/coupled.png)

### Heterogeneous growth of a plant tissue (figure 3)

Heterogeneous pressure, material property or cell geometry fields can be defined in the files _definitions/param\_Fields*.cpp_. For instance, a linear gradient pressure field can be defined in the file _definitions/param\_Fields\_pressure.cpp_ file with the following lines

![](figures/pressure.png)

Then the microscopic and coupled simulations are lounched as in the case of homogenous tissues. Below we show the pressure pattern we defined as well as the last snapshot of the grown tissue both as the result of the microscopic and the coupled simulation.

![](figures/gradient.png)

This is the way data and snapshots for figure 3 of the manuscript can be generated.

## License

**PlantHom** is a free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation. You should have received a copy of the GNU Lesser General Public License along with **PlantHom**.  If not, see <http://www.gnu.org/licenses/>.

## Citation

If you use PlantHom, please cite the publication
[_Arezki Boudaoud, Annamaria Kiss, Mariya Ptashnyk 
**Multiscale Modelling and Analysis of Growth of Plant Tissues**_. SIAM Journal on Applied Mathematics, 2023, 83 (6), pp.2354-2389. ⟨10.1137/23M1553315⟩. ⟨hal-04334494⟩](https://hal.science/hal-04334494)



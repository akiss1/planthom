#! /usr/bin/python3

from sys import path, argv
import os
import numpy as np
import pandas as pd
from math import sin, cos, pi

# multiprocessing
from multiprocessing import Pool
nproc = 15 # number of processors

def strt(t):
	return "{:.2f}".format(t)

exec(open("./parameters.py").read())

# ======== output directories ====

# outputdir
outputdir="CoupledSimulation_wall"+str(wall0)+"_gtype"+str(growthtype)

os.system("mkdir "+outputdir)
os.system("scp parameters.py "+outputdir)

# logfile
logname = outputdir+"/coupled_simulation.log"
os.system("date>"+logname)

# macroscopic results
Macrodirname=outputdir+"/Macroresults"
os.system("mkdir "+Macrodirname)

# Unit Cell results
UCdirname=outputdir+"/UCresults"
os.system("mkdir "+UCdirname)



print("time integration parameters:")
print("tmax=",tmax,"   Nt=",Ntpoints,"   dt=",dt)
log=open(logname,"a")
log.write("time integration parameters: tmax="+str(tmax)+"   Nt="+str(Ntpoints)+"   dt="+strt(dt)+"\n")
log.close()


# write timestamps in files
tstamps=[strt(t) for t in np.linspace(0,tmax,Ntpoints)]
tstamps.append('end')

# ======== initialisation =========

t=0.
os.system("FreeFem++ macroscopic_init.cpp -macrodir "+Macrodirname+" -nx "+str(nxmacro)+" -L "+str(L)+" -H "+str(H)+" -Ecw "+str(Ecw)
			+" -l1 "+str(l10)+" -l2 "+str(l20)+" -wall "+str(wall0)
           + " -coarse_edge "+str(coarse_edge))

# read one EF value file on the coarse mesh -> find out the number of vertices
f=open(Macrodirname+"/Fg0_t0.00.txt","r")
firstline=f.readline()
f.close()
Nvertex = int(firstline.split("\t")[0])
log=open(logname,"a")
log.write("Number of vertices in the coarse mesh:"+str(Nvertex)+"\n")
log.close()


def construct_coarsemesh_valuefiles(Nb, A, UCdata, t): 
	# construct coarse mesh value files for a tensor A with Nb elements
	for k in range(Nb):
		f=open(Macrodirname+"/"+A+str(k)+"_t"+strt(t)+".txt","w")
		f.write(str(Nvertex))
		for j in range(Nvertex):
			Khomk=float(UCdata[j][A+str(k)])
			if (j%5==0):
				f.write('\n\t')
			f.write(str(Khomk)+' ')	
		f.close()
	return


def construct_coarsemesh_valuefiles_scalar(A, UCdata, t): 
	# construct coarse mesh value files for a scalar A
	f=open(Macrodirname+"/"+A+"_t"+strt(t)+".txt","w")
	f.write(str(Nvertex))
	for j in range(Nvertex):
		Khomk=float(UCdata[j][A])
		if (j%5==0):
			f.write('\n\t')
		f.write(str(Khomk)+' ')	
	f.close()
	return


def read_coarsemesh_valuefiles(fname):
	f=open(fname,"r")
	vals=f.read()
	f.close()
	return [float(val) for val in vals.split("\t")[2:-1]]

# ======== time iterations =========

for i in range(Ntpoints):
	log=open(logname,"a")	
	log.write("===================i="+str(i)+" t="+str(t)+" dt="+str(dt)+"  =======================\n")
	log.close()
	# Results of unit cell problems at each vertex of the coarse mesh will be saved in :
	UCdirname=outputdir+"/UCresults/t"+strt(t)
	os.system("mkdir "+UCdirname)
	# Read Fg tensor elements for each vertex of the coarse mesh
	Fg=[]
	for k in np.arange(4):
		fgfilename=Macrodirname+"/Fg"+str(k)+"_t"+strt(t)+".txt"
		f=open(fgfilename,"r")
		vals=f.read()
		f.close()
		vals=[float(val) for val in vals.split("\t")[2:-1]]	
		Fg.append(vals)
	# read wallthickness values on the coarse mesh (w)
	wallCM=read_coarsemesh_valuefiles(Macrodirname+"/wall.txt")
	# read l2 values on the coarse mesh (cell size)
	l2CM=read_coarsemesh_valuefiles(Macrodirname+"/l2.txt")
	# read l1/l2 values on the coarse mesh (related to cell anisotropy)
	l1pl2CM=read_coarsemesh_valuefiles(Macrodirname+"/l1pl2.txt")
	# read Ecw values on the coarse mesh
	EcwCM=read_coarsemesh_valuefiles(Macrodirname+"/Ecw.txt")
	# read nu values on the coarse mesh
	nuCM=read_coarsemesh_valuefiles(Macrodirname+"/nu.txt")
	# read XY values on the coarse mesh
	XY=[]
	for k in np.arange(2):
		XY.append(read_coarsemesh_valuefiles(Macrodirname+"/X"+str(k)+".txt"))
	list_params=[ [str(v), UCdirname, [Fg[0][v], Fg[1][v], Fg[2][v], Fg[3][v]], EcwCM[v], nuCM[v], [XY[0][v], XY[1][v]], wallCM[v], l2CM[v], l1pl2CM[v]] for v in range(Nvertex)]
	# -------------------
	# Solve unit cell problems at each vertex of the coarse mesh
	# --> write results in UCdirname
	def one_UCproblem(param):
		[UClabel, UCdirname, fg, EcellWall, nuCellWall, XY, wallv, l2v, l1pl2v]=param
		cm_string="FreeFem++ unitCell.cpp -uc "+UClabel+" -nx "+str(nxUC)
		cm_string=cm_string+" -outdir "+UCdirname+" -l1 "+str(l1pl2v*l2v)+" -l2 "+str(l2v)+" -wall "+str(wallv)+" -theta "+str(theta)
		cm_string=cm_string+" -Ecw "+str(EcellWall)+" -nu "+str(nuCellWall)
		for k in range(4):
			cm_string=cm_string+" -Fg"+str(k)+" "+str(fg[k])
		for k in range(2):
			cm_string=cm_string+" -X"+str(k)+" "+str(XY[k])
		os.system(cm_string)
	#
	pool = Pool(processes=nproc)
	pool.map(one_UCproblem, list_params)
	pool.close()
	pool.join()
	# read results of the UC problems
	UCdata=[]
	for j in range(Nvertex):
		UCdata.append(pd.read_csv(UCdirname+'/UC'+str(j)+'.csv',delimiter=';'))
	# -----------------
	# construct coarse mesh value files for UnitCell volume Y
	construct_coarsemesh_valuefiles_scalar("Y", UCdata, t)
	# construct coarse mesh value files for wall volume Yw in UnitCell
	construct_coarsemesh_valuefiles_scalar("Yw", UCdata, t)
	# construct coarse mesh value files for Ehom
	construct_coarsemesh_valuefiles(6, "Ehom", UCdata, t)
	# construct coarse mesh value files for Khom
	construct_coarsemesh_valuefiles(4, "Khom", UCdata, t)
	# construct coarse mesh value files for Eaux
	construct_coarsemesh_valuefiles(6, "Eaux", UCdata, t)
	# construct coarse mesh value files for Kaux
	construct_coarsemesh_valuefiles(4, "Kaux", UCdata, t)
	# construct coarse mesh value files for P2vect
	construct_coarsemesh_valuefiles(2, "P2vect", UCdata, t)
	# ----------------------
	# solve the macroscopic problem
	log=open(logname,"a")
	texte="Solving macroscopic problem at t="+str(t)+" (dt="+str(dt)+")\n"
	log.write(texte)
	log.close()
	print(texte)
	cm_string="FreeFem++ macroscopic_tissue-growth_1step.cpp -t "+str(t)+" -dt "+str(dt)+" -tstamp "+tstamps[i]+" -tstamp1 "+tstamps[i+1]
	cm_string=cm_string+" -gt "+str(growthtype)
	cm_string=cm_string+" -P0 "+str(P0)+" -Ecw "+str(Ecw)
	cm_string=cm_string+" -macrodir "+Macrodirname + " -coarse_edge "+str(coarse_edge)
	cm_string=cm_string+" -L "+str(L)+" -H "+str(H)+" -nx "+str(nxmacro)+" -log "+str(logname)
	cm_string=cm_string+" -Y Y"+"_t"+strt(t)+".txt"
	cm_string=cm_string+" -Yw Yw"+"_t"+strt(t)+".txt"
	cm_string=cm_string+" -sigmaC "+str(sigmaC)+" -sigmaThreshold "+str(sigmaThreshold)
	for k in range(4):
		cm_string=cm_string+" -Fg"+str(k)+" Fg"+str(k)+"_t"+strt(t)+".txt"
	for k in range(4):
		cm_string=cm_string+" -Khom"+str(k)+" Khom"+str(k)+"_t"+strt(t)+".txt"
	for k in range(4):
		cm_string=cm_string+" -Kaux"+str(k)+" Kaux"+str(k)+"_t"+strt(t)+".txt"
	for k in range(6):
		cm_string=cm_string+" -Ehom"+str(k)+" Ehom"+str(k)+"_t"+strt(t)+".txt"
	for k in range(6):
		cm_string=cm_string+" -Eaux"+str(k)+" Eaux"+str(k)+"_t"+strt(t)+".txt"
	for k in range(2):
		cm_string=cm_string+" -P2vect"+str(k)+" P2vect"+str(k)+"_t"+strt(t)+".txt"
	log=open(logname,"a")
	log.write(cm_string+"\n")
	log.close()
	os.system(cm_string)
	log=open(logname,"a")
	log.write("Macroscopic problem at t="+str(t)+" solved. \n")
	log.close()	
	t=t+dt




'''
pool = Pool(processes=nproc)
pool.map(one_UCproblem, list_params)
pool.close()
pool.join()
'''

#! /usr/bin/python3

from sys import path, argv
import os
import numpy as np
import pandas as pd
from math import sin, cos, pi


# multiprocessing
from multiprocessing import Pool
nproc = 11 #number of processors

def strt(t):
	return "{:.2f}".format(t)

exec(open("./parameters.py").read())


print("time integration parameters:")
print("tmax=",tmax,"   Nt=",Ntpoints,"   dt=",dt)

# logfile
#logname = "microscopic_simulation.log"
#os.system("echo ========================================>>"+logname)
#os.system("date>>"+logname)

cmd="FreeFem++ microscopic_tissue-growth.cpp "
cmd=cmd+" -gt "+str(growthtype)
cmd=cmd+" -nvertex "+str(nx)+" -nxmacro "+str(nxmacro)
cmd=cmd+" -tmax "+str(tmax)+" -nt "+str(Ntpoints)
cmd=cmd+" -Nx "+str(Nx)+" -Ny "+str(Ny)
cmd=cmd+" -l1 "+str(l1)+" -l2 "+str(l2)+" -theta "+str(theta)+" -wall "+str(wall)+" -extwall "+str(extwall)
cmd=cmd+" -L "+str(L)+" -H "+str(H)
cmd=cmd+" -P0 "+str(P0)+" -Ecw "+str(Ecw)+" -nu "+str(nu)+" -sigmaThreshold "+str(sigmaThreshold)+" -sigmaC "+str(sigmaC)
#os.system("echo "+cmd+">>"+logname)
os.system(cmd)

//bool debug = true;
include "getARGV.idp"

// Unit Cell problem
// - periodic boundary conditions
// - plane stress conditions -> 2D


/*************************************************************
 *                  PARAMETERS
 * **********************************************************/
 
 
int help=getARGV("-help", 0);

if (help) {
	cout<<"================== Unit Cell problem ==================="<<endl;
	cout<<"Usage:"<<endl;
	cout<<"FreeFem++ unitCell.cpp [PARAMETERS]"<<endl<<endl;
	cout<<"Model parameters:"<<endl;
	cout<<"   --- Geometry ---"<<endl;
	cout<<"-l1 (default:1)"<<endl;	cout<<"-l2 (default:1)"<<endl;	cout<<"-theta (default:30)"<<endl;cout<<"-wall (default:0.05)"<<endl;
	cout<<"   --- Elasticity ---"<<endl;
	cout<<"-Ecw (default:2)"<<endl;	cout<<"-nu (default:0.3)"<<endl;
	cout<<"   --- Growth tensor Fg ---"<<endl;
	cout<<"-Fg0 (default:1)"<<endl;	cout<<"-Fg1 (default:0)"<<endl;cout<<"-Fg2 (default:0)"<<endl;	cout<<"-Fg3 (default:1)"<<endl;
}
else {
 
int alone = getARGV("-alone", 0);

if (alone) {
	cout<<"================== One unitcell problem alone ==================="<<endl;
}

// Unit Cell label
int UClabel = getARGV("-uc", 0);
string outputdir = getARGV("-outdir", ".");
string outputfile = outputdir+"/UC"+string(UClabel)+".csv";

// time variables
int anew = getARGV("-newmesh", 0);

// growth
real Fg11 = getARGV("-Fg0", 1.);  // components of Fg
real Fg12 = getARGV("-Fg1", 0.);
real Fg21 = getARGV("-Fg2", 0.);
real Fg22 = getARGV("-Fg3", 1.);

// cell geometry
real l1=getARGV("-l1", 1.);
real l2=getARGV("-l2", 1.);
real theta=getARGV("-theta", 30)*pi/180;

// mechanical parameters
real Ecw = getARGV("-Ecw", 2.0);  // Ecw
real nu=getARGV("-nu", 0.3);  // nu

// layered wall --> homogenious wall 
real hwall=getARGV("-wall", .05);
real wallfrac = getARGV("-wallfrac", 1.0);  // cw/w=cw/cwplusml
real Efrac = getARGV("-Efrac", 1.0);  // =Eml/Ecw
 
int nx=getARGV("-nx", 25);

real lfact=getARGV("-lfact", 1.);


// ---------------- geometrical parameters
int d=2;

// pressure
real Pressure0=getARGV("-P0", 0.001);
real L=getARGV("-L", 1);
include "definitions/param_Fields_pressure.cpp";
real X=getARGV("-X", 0.5);
real Y=getARGV("-Y", 0.5);

// compute P2vect on a UC arround (X,Y)
real Pcenter=Pressure(X,Y);

real lambda1=4*l2*cos(theta), lambda2=2*(l1+l2*sin(theta)); // UC dimensions
real unitcellvolume=lambda1*lambda2;

real delta=1.0;
real p=1.0;


real Eml=Ecw*Efrac;

int nvertex=nx;


include "visualisation/boundingboxes_cell.cpp";


//coeficient of displacement amplification for plotting deformed tissue
rho=.2;

// dependent parameters
//real hwall=ml+cw;

/*************************************************************
 *                  GEOMETRY and MESH
 * **********************************************************/

include "definitions/bib_general.cpp";
include "geometry/geometry_unitCell.cpp";
include "definitions/bib_fe_unitCell.cpp";
include "definitions/bib_tensors.cpp";


if (alone){
plot(ThTissue,  ps=outputdir+"/UC_geometry-original.png",  wait=true);
}



// 2. The wall-angles
Sh0 thetah=0;


// ---------- parameters ----------
cout<<"============================================"<<endl;
real volume=int2d(ThTissue)(1.);
cout<< "Wall volume = "<<volume << endl;
cout<< "Unit cell volume = "<<unitcellvolume<<endl;

macro meanUC(X)   int2d(ThTissue)(X)/unitcellvolume //


Sh0 EYoung=0.0;
EYoung=Ecw;

// 2D or plane stress formulas
//cout<<"2D or plane stress elastic tensor"<<endl;
Sh0 mu=EYoung/(1+nu)/2, lambda=EYoung*nu/(1+nu)/(1-nu);

// elasticity matrix
//macro E0 [2*mu+lambda, 2*mu+lambda, lambda, mu, 0, 0] //
//E=E0;

E=Etensor(mu, lambda);


// external force
macro f [f1, f2] //
Vh2 f;
f=[.2, 0];

// growth defined as input
Fg=[Fg11, Fg12, Fg21, Fg22];

/*
// growth - anisotropic
Fg=Rtensor([g, 0., 0., 1.],alpha);


// growth - isotropic
Fg=Rtensor([g, 0., 0., g],alpha);
*/

// Fg is at this stage a homogeneous tensor field on the unit cell
// *** project Fg on the walls --> the projected Fg is a nonhomogeneous field on the unit cell

/*
Sh0 growth=Rtensor(Fg,-thetah)[0]-1;
growth=absplus(growth);
real meangrowth=meanWall(growth);
cout<<"meangrowth="<<meangrowth<<endl;
//plot(growth,value=true,fill=1, ps="growth.eps",bb=bbtissue, wait=true, cmm="growth");
Fg=Rtensor([growth+1,0,0,1],thetah);
$/

/*
plot(Fg[0],value=true,fill=1, ps="Fg11.eps",bb=bbtissue, wait=true, cmm="Fg11");
plot(Fg[1],value=true,fill=1, ps="Fg12.eps",bb=bbtissue, wait=true, cmm="Fg12");
plot(Fg[2],value=true,fill=1, ps="Fg21.eps",bb=bbtissue, wait=true, cmm="Fg21");
plot(Fg[3],value=true,fill=1, ps="Fg22.eps",bb=bbtissue, wait=true, cmm="Fg22");
*/


macro A11 [1., 0., 0., 0.] //
macro A22 [0., 0., 0., 1.] //
macro A12 [0., .5, .5, 0.] //

macro unitcellforce(Em,A,v,Fg)     Jg(Fg)*tensprod(Em,A,graduFg1sym(v,Fg)) //
macro DeltaE(Em, wij, kl)   int2d(ThTissue)(tensmatprod(Em,graduFg1sym(wij,Fg))[kl])/unitcellvolume //



// solution displacement fields :

macro w11 [w111, w112] //
Vh2 w11;
macro w12 [w121, w122] //
Vh2 w12;
macro w22 [w221, w222] //
Vh2 w22;

macro w [w1, w2] //
Vh2 w;

real translx, transly;


// ===========================
cout<<"Applying A11..."<<endl;
// ===========================
problem ucell(u,v) = int2d(ThTissue)( elasticity1(E,u,Fg,v) ) //, solver=LU
			+ int2d(ThTissue)( unitcellforce(E,A11,v,Fg) )
			;

ucell;
w11=u;

/*
plot(u1, value=true,fill=1,  ps=rootdef+"u1_A11.png", bb=bbtissue, wait=false);
plot(u2, value=true,fill=1,  ps=rootdef+"u2_A11.png", bb=bbtissue, wait=false);
*/

if (alone){
translx=u1(2*l2*cos(theta),l1+l2*cos(theta));
transly=u2(2*l2*cos(theta),l1+l2*cos(theta));
Thm=movemesh(ThTissue, [x+rho*(u[0]-translx), y+rho*(u[1]-transly)]);
plot(Thm,  ps=outputdir+"/UC_geometry-deformed11.png",  wait=true);
}

{
// ===========================
cout<<"Applying A12..."<<endl;
// ===========================
problem ucell(u,v) = int2d(ThTissue)( elasticity1(E,u,Fg,v) ) //, solver=LU
			+ int2d(ThTissue)( unitcellforce(E,A12,v,Fg) )
			;
ucell;
w12=u;
}

/*
plot(u1, value=true,fill=1,  ps=rootdef+"u1_A12.png", bb=bbtissue, wait=false);
plot(u2, value=true,fill=1,  ps=rootdef+"u2_A12.png", bb=bbtissue, wait=false);
*/

if (alone){
translx=u1(2*l2*cos(theta),l1+l2*cos(theta));
transly=u2(2*l2*cos(theta),l1+l2*cos(theta));
Thm=movemesh(ThTissue, [x+rho*(u[0]-translx), y+rho*(u[1]-transly)]);
plot(Thm,  ps=outputdir+"/UC_geometry-deformed12.png",  wait=true);
}

{
// ===========================
cout<<"Applying A22..."<<endl;
// ===========================
problem ucell(u,v) = int2d(ThTissue)( elasticity1(E,u,Fg,v) ) //, solver=LU
			+ int2d(ThTissue)( unitcellforce(E,A22,v,Fg) )
			;
ucell;
w22=u;
}

/*
plot(u1, value=true,fill=1,  ps=rootdef+"u1_A22.png", bb=bbtissue, wait=false);
plot(u2, value=true,fill=1,  ps=rootdef+"u2_A22.png", bb=bbtissue, wait=false);
*/

if (alone){
translx=u1(2*l2*cos(theta),l1+l2*cos(theta));
transly=u2(2*l2*cos(theta),l1+l2*cos(theta));
Thm=movemesh(ThTissue, [x+rho*(u[0]-translx), y+rho*(u[1]-transly)]);
plot(Thm,  ps=outputdir+"/UC_geometry-deformed22.png", wait=true);
}



// means on the unit cell
real mmeanE0 =meanUC(E[0]);
real mmeanE1 =meanUC(E[1]);
real mmeanE2 =meanUC(E[2]);
real mmeanE3 =meanUC(E[3]);
real mmeanE4 =meanUC(E[4]);
real mmeanE5 =meanUC(E[5]);


real effE0 =mmeanE0+DeltaE(E, w11, 0);
real effE1 =mmeanE1+DeltaE(E, w22, 3);
real effE2 =mmeanE2+DeltaE(E, w11, 3);
real effE3 =mmeanE3+DeltaE(E, w12, 1);
real effE4 =mmeanE4+DeltaE(E, w11, 1);
real effE5 =mmeanE5+DeltaE(E, w22, 1);


macro effE [effE0, effE1, effE2, effE3, effE4, effE5]//
macro mmeanE [mmeanE0, mmeanE1, mmeanE2, mmeanE3, mmeanE4, mmeanE5]//



// ===========================
cout<<"Computing Khom..."<<endl;
// ===========================
macro unitcellforceK(u,Fg)     Jg(Fg)*trace(graduFg1(u,Fg)) //

{
problem ucell(u,v) = int2d(ThTissue)( elasticity1(E,u,Fg,v) ) 
			+ int2d(ThTissue)( unitcellforceK(v,Fg) )
			;
ucell;
w=u;
}

/*
plot(u1, value=true,fill=1,  ps=rootdef+"u1_I.png", bb=bbtissue, wait=true);
plot(u2, value=true,fill=1,  ps=rootdef+"u2_I.png", bb=bbtissue, wait=true);
*/

if (alone){
translx=u1(2*l2*cos(theta),l1+l2*cos(theta));
transly=u2(2*l2*cos(theta),l1+l2*cos(theta));
rho=1;
Thm=movemesh(ThTissue, [x+rho*(u[0]-translx), y+rho*(u[1]-transly)]);
plot(Thm,  ps=outputdir+"/UC_geometry-deformedI"+string(rho)+".png", wait=true);
}


real Khom11 =DeltaE(E, w, 0);
real Khom12 =DeltaE(E, w, 1);
real Khom21 =DeltaE(E, w, 2);
real Khom22 =DeltaE(E, w, 3);


cout<< endl;
cout<< "Ehom0="  << effE0 << endl;
cout<< "Ehom1="  << effE1 << endl;
cout<< "Ehom2="  << effE2 << endl;
cout<< "Ehom3="  << effE3 << endl;
cout<< "Ehom4="  << effE4 << endl;
cout<< "Ehom5="  << effE5 << endl;

cout<< endl;
cout<< "Khom11="  << Khom11 << endl;
cout<< "Khom12="  << Khom12 << endl;
cout<< "Khom21="  << Khom21 << endl;
cout<< "Khom22="  << Khom22 << endl;


// compute Eaux
macro E0aux [1., 1., 0, 1./2., 0, 0] //

real Eaux0 =meanUC(E0aux[0])+DeltaE(E0aux, w11, 0);
real Eaux1 =meanUC(E0aux[1])+DeltaE(E0aux, w22, 3);
real Eaux2 =meanUC(E0aux[2])+DeltaE(E0aux, w11, 3);
real Eaux3 =meanUC(E0aux[3])+DeltaE(E0aux, w12, 1);
real Eaux4 =meanUC(E0aux[4])+DeltaE(E0aux, w11, 1);
real Eaux5 =meanUC(E0aux[5])+DeltaE(E0aux, w22, 1);

// compute Kaux

real Kaux0 =DeltaE(E0aux, w, 0);
real Kaux1 =DeltaE(E0aux, w, 1);
real Kaux2 =DeltaE(E0aux, w, 2);
real Kaux3 =DeltaE(E0aux, w, 3);


// compute P2vect

Thm=movemesh(ThTissue, [x+X, y+Y]);
fespace Sh0m(Thm,P1);

Sh0 P2h=(Pcenter-Pressure(x,y))*L/lambda1;

real P2vect0=int1d(Thm,internal)(P2h*N.x)/unitcellvolume;
real P2vect1=int1d(Thm,internal)(P2h*N.y)/unitcellvolume;


// printing results
// ================
string header="";
string values="";

real ml=0, cw=hwall;

header=header+"ml;"; values=values+string(ml)+";";
header=header+"cw;"; values=values+string(cw)+";";
header=header+"Eml;"; values=values+string(Eml)+";";
header=header+"Ecw;"; values=values+string(Ecw)+";";
header=header+"nu;"; values=values+string(nu)+";";

header=header+"Ehom0;"; values=values+string(effE0)+";";
header=header+"Ehom1;"; values=values+string(effE1)+";";
header=header+"Ehom2;"; values=values+string(effE2)+";";
header=header+"Ehom3;"; values=values+string(effE3)+";";
header=header+"Ehom4;"; values=values+string(effE4)+";";
header=header+"Ehom5;"; values=values+string(effE5)+";";


header=header+"nx;"; values=values+string(nx)+";";
header=header+"nvertex;"; values=values+string(nvertex)+";";
header=header+"l1;"; values=values+string(l1)+";";
header=header+"l2;"; values=values+string(l2)+";";
header=header+"lfact;"; values=values+string(lfact)+";";
header=header+"theta;"; values=values+string(theta*180/pi)+";";
header=header+"wfrac;"; values=values+string(wallfrac)+";";
header=header+"Efrac;"; values=values+string(Efrac)+";";


header=header+"Khom0;"; values=values+string(Khom11)+";";
header=header+"Khom1;"; values=values+string(Khom12)+";";
header=header+"Khom2;"; values=values+string(Khom21)+";";
header=header+"Khom3;"; values=values+string(Khom22)+";";

header=header+"P2vect0;"; values=values+string(P2vect0)+";";
header=header+"P2vect1;"; values=values+string(P2vect1)+";";

header=header+"Yw;"; values=values+string(volume)+";";
header=header+"Y;"; values=values+string(unitcellvolume)+";";

header=header+"Eaux0;"; values=values+string(Eaux0)+";";
header=header+"Eaux1;"; values=values+string(Eaux1)+";";
header=header+"Eaux2;"; values=values+string(Eaux2)+";";
header=header+"Eaux3;"; values=values+string(Eaux3)+";";
header=header+"Eaux4;"; values=values+string(Eaux4)+";";
header=header+"Eaux5;"; values=values+string(Eaux5)+";";

header=header+"Kaux0;"; values=values+string(Kaux0)+";";
header=header+"Kaux1;"; values=values+string(Kaux1)+";";
header=header+"Kaux2;"; values=values+string(Kaux2)+";";
header=header+"Kaux3;"; values=values+string(Kaux3)+";";




ofstream textfile(outputfile);
textfile<<header<<endl;
textfile<<values<<endl;


}// end of if for the help

bool debug = true;
include "getARGV.idp"
load "iovtk"

/*************************************************************
 *                  PARAMETERS
 * **********************************************************/
int gt = getARGV("-gt", 1);  // growth type (0 for strain based and 1 for stress based)

bool initialisation = getARGV("-init", 1);

int nx=getARGV("-nx", 25);
real coarseedge=getARGV("-coarse_edge", 1.);

// time variables
real t=getARGV("-t", 0.);
real dt=getARGV("-dt", 0.1);
string Macrodirname=getARGV("-macrodir", ".");
real sigmaC=getARGV("-sigmaC", 1.);
real L=getARGV("-L", 1.);
real H=getARGV("-H", 1./2.);

real Pressure0=getARGV("-P0",0.);
real sigmaThreshold=getARGV("-sigmaThreshold", 0.);
real Ecw=getARGV("-Ecw", 1.);


//real Y=getARGV("-Y",1.);
//real Yw=getARGV("-Yw",1.);


string tstamp=getARGV("-tstamp", "0.00");
string tstamp1=getARGV("-tstamp1", "0.00");

//coeficient of displacement amplification for plotting deformed tissue
real rho=1;

// --- geometrical parameters
// ----------------------------
include "definitions/bib_general.cpp";
include "geometry/geometry_macroscopic.cpp";
include "definitions/bib_fe_macroscopic.cpp";
include "definitions/bib_tensors.cpp";
include "visualisation/colormaps.cpp";
include "visualisation/boundingboxes_tissue.cpp";

/*
plot(ThTissue, ps=Macrodirname+"tissue_geometry.eps", bb=bbtissue, wait=false);
*/

// --- cell size --- Y and Yw fields
// ----------------------------

string Yfname = Macrodirname+"/"+getARGV("-Y", "Y.txt");
VhC Y;
{ifstream g(Yfname); g >> Y[];}

string Ywfname = Macrodirname+"/"+getARGV("-Yw", "Yw.txt");
VhC Yw;
{ifstream g(Ywfname); g >> Yw[];}


// --- pressure
// ----------------------------
include "definitions/param_Fields_pressure.cpp";

Sh0 Ph;
Ph=Pressure;
plot(Ph, cmm="P", value=true,fill=1, ps=Macrodirname+"/pressure.eps",bb=bbtissue, wait=false, hsv=HSVviridis);

// --- extensibility, stress threshold
// ----------------------------
include "definitions/param_Fields_growth.cpp";

Sh0 sigmaCh;
sigmaCh=Cfunc;
plot(sigmaCh, cmm="sigmaC", value=true,fill=1, ps=Macrodirname+"/sigmaC.eps",bb=bbtissue, wait=false, hsv=HSVviridis);


Sh0 sigmaThresholdh;
sigmaThresholdh=Tfunc;
plot(sigmaThresholdh, cmm="sigmaThreshold", value=true,fill=1, ps=Macrodirname+"/sigmaThreshold.eps",bb=bbtissue, wait=false, hsv=HSVviridis);


// --- Ehom elasticity tensor
// ----------------------------
string Ehom0 = Macrodirname+"/"+getARGV("-Ehom0", "Ehom0.txt");
string Ehom1 = Macrodirname+"/"+getARGV("-Ehom1", "Ehom1.txt");
string Ehom2 = Macrodirname+"/"+getARGV("-Ehom2", "Ehom2.txt");
string Ehom3 = Macrodirname+"/"+getARGV("-Ehom3", "Ehom3.txt");
string Ehom4 = Macrodirname+"/"+getARGV("-Ehom4", "Ehom4.txt");
string Ehom5 = Macrodirname+"/"+getARGV("-Ehom5", "Ehom5.txt");

cout << "reading homogenized elasticity tensor"<<endl;

VhC Ehom0C, Ehom1C, Ehom2C, Ehom3C, Ehom4C, Ehom5C;
{ifstream g(Ehom0); g >> Ehom0C[];}
{ifstream g(Ehom1); g >> Ehom1C[];}
{ifstream g(Ehom2); g >> Ehom2C[];}
{ifstream g(Ehom3); g >> Ehom3C[];}
{ifstream g(Ehom4); g >> Ehom4C[];}
{ifstream g(Ehom5); g >> Ehom5C[];}

E=[Ehom0C, Ehom1C, Ehom2C, Ehom3C, Ehom4C, Ehom5C];

// --- Khom
// ----------------------------

string K011 = Macrodirname+"/"+getARGV("-Khom0", "Khom0.txt");
string K012 = Macrodirname+"/"+getARGV("-Khom1", "Khom1.txt");
string K021 = Macrodirname+"/"+getARGV("-Khom2", "Khom2.txt");
string K022 = Macrodirname+"/"+getARGV("-Khom3", "Khom3.txt");

VhC Khom11C, Khom12C, Khom21C, Khom22C;
{ifstream g(K011); g >> Khom11C[];}
{ifstream g(K012); g >> Khom12C[];}
{ifstream g(K021); g >> Khom21C[];}
{ifstream g(K022); g >> Khom22C[];}

K=[Khom11C, Khom12C, Khom21C, Khom22C];


// --- Eaux tensor
// ----------------------------
string Eaux0 = Macrodirname+"/"+getARGV("-Eaux0", "Eaux0.txt");
string Eaux1 = Macrodirname+"/"+getARGV("-Eaux1", "Eaux1.txt");
string Eaux2 = Macrodirname+"/"+getARGV("-Eaux2", "Eaux2.txt");
string Eaux3 = Macrodirname+"/"+getARGV("-Eaux3", "Eaux3.txt");
string Eaux4 = Macrodirname+"/"+getARGV("-Eaux4", "Eaux4.txt");
string Eaux5 = Macrodirname+"/"+getARGV("-Eaux5", "Eaux5.txt");

VhC Eaux0C, Eaux1C, Eaux2C, Eaux3C, Eaux4C, Eaux5C;
{ifstream g(Eaux0); g >> Eaux0C[];}
{ifstream g(Eaux1); g >> Eaux1C[];}
{ifstream g(Eaux2); g >> Eaux2C[];}
{ifstream g(Eaux3); g >> Eaux3C[];}
{ifstream g(Eaux4); g >> Eaux4C[];}
{ifstream g(Eaux5); g >> Eaux5C[];}

Eaux=[Eaux0C, Eaux1C, Eaux2C, Eaux3C, Eaux4C, Eaux5C];

// --- Kaux tensor
// ----------------------------

K011 = Macrodirname+"/"+getARGV("-Kaux0", "Kaux0.txt");
K012 = Macrodirname+"/"+getARGV("-Kaux1", "Kaux1.txt");
K021 = Macrodirname+"/"+getARGV("-Kaux2", "Kaux2.txt");
K022 = Macrodirname+"/"+getARGV("-Kaux3", "Kaux3.txt");

VhC Kaux11C, Kaux12C, Kaux21C, Kaux22C;
{ifstream g(K011); g >> Kaux11C[];}
{ifstream g(K012); g >> Kaux12C[];}
{ifstream g(K021); g >> Kaux21C[];}
{ifstream g(K022); g >> Kaux22C[];}

Kaux=[Kaux11C, Kaux12C, Kaux21C, Kaux22C]; 

// --- Fg growth tensor
// ----------------------------
string Fg011 = Macrodirname+"/"+getARGV("-Fg0", "Fg0.txt");
string Fg012 = Macrodirname+"/"+getARGV("-Fg1", "Fg1.txt");
string Fg021 = Macrodirname+"/"+getARGV("-Fg2", "Fg2.txt");
string Fg022 = Macrodirname+"/"+getARGV("-Fg3", "Fg3.txt");

VhC Fg11C, Fg12C, Fg21C, Fg22C;

cout << "reading Fg tensor elements at t="<<tstamp1<<endl;

{ifstream g(Fg011); g >> Fg11C[];}
{ifstream g(Fg012); g >> Fg12C[];}
{ifstream g(Fg021); g >> Fg21C[];}
{ifstream g(Fg022); g >> Fg22C[];}	

Fg=[Fg11C, Fg12C, Fg21C, Fg22C];



// --- P2vect pressure contribution vectorfield
// ----------------------------
string P2vect0 = Macrodirname+"/"+getARGV("-P2vect0", "P2vect0.txt");
string P2vect1 = Macrodirname+"/"+getARGV("-P2vect1", "P2vect1.txt");

VhC P2vect0C, P2vect1C;

{ifstream g(P2vect0); g >> P2vect0C[];}
{ifstream g(P2vect1); g >> P2vect1C[];}

macro P2vect [PP0, PP1] //

Vh2 P2vect;
P2vect=[P2vect0C, P2vect1C];

plot(P2vect0C, cmm="P2vect0", value=true,fill=1, ps=Macrodirname+"/P2vect0_coarse"+stringt(t)+".eps",bb=bbtissue, wait=false, hsv=HSVviridis);
plot(P2vect1C, cmm="P2vect1", value=true,fill=1, ps=Macrodirname+"/P2vect1_coarse"+stringt(t)+".eps",bb=bbtissue, wait=false, hsv=HSVviridis);

// plot trace(Fg) on the coarse mesh
Fg11C=Fg11C+Fg22C;
plot(Fg11C, value=true,fill=1, ps=Macrodirname+"/TrFg_coarse"+stringt(t)+".eps",bb=bbtissue, wait=false, hsv=HSVviridis);

// plot trace(Fg) on the fine mesh
Sh0 field=Fg[0]+Fg[3];
plot(field, value=true,fill=1, ps=Macrodirname+"/TrFg_fine"+stringt(t)+".eps",bb=bbtissue, wait=false, hsv=HSVviridis);


// stress
// ----------------------------
Sh0 sig, sig0, sig1, sig2, sig3;
Sh0 epsilon11;

// external force
// ----------------------------
macro f [f1, f2] //
Vh2 f;
f=[.2, 0];


//int n0 = Sh2.ndof;
//cout << "n0="<<n0<<endl;

//int n1=u1[].n;
//int n2=u2[].n;



Sh0 zeta=1-Yw/Y;

// Definition of the problem
// ===========================
varf va(u,v) = int2d(ThTissue)( elasticity1(E,u,Fg,v)  ) // bilinear form
			+ on(10, u2=0) 
			//+ on(40, u1=0)
			; 

varf vL(u,v) = 	- int2d(ThTissue)( elasticity0(E,Fg,v)  )   // linear form    
			- int2d(ThTissue)( pressurefield(K,Ph,Fg,v) )
			+ int2d(ThTissue)( zeta*pressurefield0(Ph,Fg,v) )
			+ int2d(ThTissue)( zeta*gradpressurefield(Ph,Fg,v) )
			- int2d(ThTissue)( P2term(Fg, P2vect, v) );

varf vb(u,v) = int2d(ThTissue)(1.*v1); // linear form for introducing the constraint int(u1)=0 by Lagrange multiplier


// build the block matrix for the lhs
int n = Vh2.ndof;
cout << "n="<<n<<endl;

matrix A=va(Vh2,Vh2);

real[int] b(n);
b = vL(0,Vh2);

real[int] sol(n);

//sol = A^-1*b;
//u1[]=sol; // causes the copy of u2 also (!!!!)
//cout<<"original problem solved"<<endl;

// now take into account the lagrange multiplier 
real[int]  B = vb(0,Vh2);

matrix AA = [ [ A ,  B ] ,
              [ B', 0 ] ] ;

// build the block matrix for the rhs
real[int]  bb(n+1),xx(n+1),b1(1),l(1);

bb = [ b, 0];

// solve the linear system
set(AA,solver=sparsesolver);
xx = AA^-1*bb; 

cout<<"LM problem solved"<<endl;

[b,l] = xx;  // set the value 
cout << " l = " << l(0) <<  " ,  b(u,1)  =" << B'*b  << endl; 


u1[]=b; // causes the copy of u2 also (!!!!) 
//(see page 72 of http://www3.freefem.org/ff++/ftp/freefem++doc.pdf)



/*
problem eleq(u,v) =
			int2d(ThTissue)( elasticity1(E,u,Fg,v)  )
			+ int2d(ThTissue)( elasticity0(E,Fg,v)  )       
			+ int2d(ThTissue)( pressurefield(K,Ph,Fg,v) )
			- int2d(ThTissue)( zeta*pressurefield0(Ph,Fg,v) )
			- int2d(ThTissue)( zeta*gradpressurefield(Ph,Fg,v) )
			+ int2d(ThTissue)( P2term(Fg, P2vect, v) )	

		+ on(10, u2=0) 
		+ on(40, u1=0) //
			;
eleq;
*/




real translx=u1(0,0);
real transly=u2(0,0);


mesh Thm1=movemesh(ThTissue, [x+rho*(u[0]-translx), y+rho*(u[1]-transly)]);
plot(ThTissue, Thm1,  ps=Macrodirname+"/"+"geometry-deformed-superposed_t"+stringt(t)+".eps", wait=false, bb=bbtissue, hsv=HSVviridis);
plot(Thm1,  ps=Macrodirname+"/"+"geometry-deformed_t"+stringt(t)+".eps", bb=bbtissue, wait=false, hsv=HSVviridis);


// compute G2 
// --------------------------------

cout<<"---------------------------------"<<endl;
cout<<"gt="<<string(gt)<<endl;
if (gt==0) {
	E=Eaux;
	K=Kaux;
	cout<<"Strain based growth hypothesis"<<endl;
	}
else {
	cout<<"Stress based growth hypothesis"<<endl;
	}
cout<<"---------------------------------"<<endl;



// plot the elements of G2

G2=stress1(E,u,Fg)+ stress0(E,Fg)+ K*Ph;
G2=Y/Yw*G2;

Sh0 thetah;
/*
thetah=G2[0];
plot(thetah, cmm="G2_0", ps=Macrodirname+"/"+"G2_0_t"+tstamp+".eps", bb=bbzoom, wait=false, value=true,fill=1, hsv=HSVviridis);

thetah=G2[1];
plot(thetah, cmm="G2_1", ps=Macrodirname+"/"+"G2_1_t"+tstamp+".eps", bb=bbzoom, wait=false, value=true,fill=1, hsv=HSVviridis);

thetah=G2[3];
plot(thetah, cmm="G2_3", ps=Macrodirname+"/"+"G2_3_t"+tstamp+".eps", bb=bbzoom, wait=false, value=true,fill=1, hsv=HSVviridis);
*/


// diagonalise G2

Sh0 TrG=trace(G2) ;
Sh0 DetG=det(G2);

Sh0 lambdaG1=(TrG+sqrt(abs(TrG*TrG-4*DetG)))/2.;
Sh0 lambdaG2=(TrG-sqrt(abs(TrG*TrG-4*DetG)))/2.;


//plot(lambdaG1, fill=true, value=true, ps=root+"lambdaG1_t"+stringt(t)+".eps", bb=bbtissue, wait=true, cmm="lambda_sigma1");
//plot(lambdaG2, fill=true, value=true, ps=root+"lambdaG2_t"+stringt(t)+".eps", bb=bbtissue, wait=true, cmm="lambda_sigma2");



thetah=-atan2((G2[0]-lambdaG1),G2[1]);

Sh0 growth1=lambdaG1-sigmaThresholdh; growth1=sigmaCh*absplus(growth1);
Sh0 growth2=lambdaG2-sigmaThresholdh; growth2=sigmaCh*absplus(growth2);

plot(growth1, cmm="growth1", ps=Macrodirname+"/"+"growth1_t"+tstamp+".eps", bb=bbtissue, wait=false, value=true,fill=1, hsv=HSVviridis);
plot(growth2, cmm="growth2", ps=Macrodirname+"/"+"growth2_t"+tstamp+".eps", bb=bbtissue, wait=false, value=true,fill=1, hsv=HSVviridis);

G2=Rtensor([growth1,0,0,growth2],thetah);


// plot the main principal direction (angle in degrees)
thetah=thetah*180/pi;
plot(thetah, cmm="Principal growth direction", value=true, fill=1, ps=Macrodirname+"/"+"theta_t"+string(t)+".eps",bb=bbtissue, wait=false, hsv=HSVviridis);

// compute growth anisotropy (overwrite on thetah in order to save memory)
Sh0 growthani=(growth1-growth2)/(growth1+growth2+1.e-13);
plot(growthani, cmm="growth anisotropy", value=true, fill=1, ps=Macrodirname+"/"+"grothani_t"+string(t)+".eps",bb=bbtissue, wait=false, hsv=HSVviridis);


// compute the trace of G2
//Sh0 tracegrowth=growth1+growth2;
Sh0 tracegrowth=G2[0]+G2[3];
plot(tracegrowth, cmm="trace(growth)", value=true, fill=1, ps=Macrodirname+"/"+"TrGrowth_t"+string(t)+".eps",bb=bbtissue, wait=false, hsv=HSVviridis);



// plot the elements of G2 after the trshold
/*
thetah=G2[0];
plot(thetah, cmm="G2[0]", ps=Macrodirname+"/"+"G2_0_t"+tstamp+".eps", bb=bbzoom, wait=false, value=true,fill=1, hsv=HSVviridis);

thetah=G2[1];
plot(thetah, cmm="G2[1]", ps=Macrodirname+"/"+"G2_1_t"+tstamp+".eps", bb=bbzoom, wait=false, value=true,fill=1, hsv=HSVviridis);

thetah=G2[3];
plot(thetah, cmm="G2[3]", ps=Macrodirname+"/"+"G3_3_t"+tstamp+".eps", bb=bbzoom, wait=false, value=true,fill=1, hsv=HSVviridis);
*/




real meantracegrowth=int2d(ThTissue)(tracegrowth)/int2d(ThTissue)(1);

string text=string(Pressure0)+";"+string(Ecw)+";"+string(sigmaC)+";"+string(sigmaThreshold)+";"+string(t)+";"+string(meantracegrowth)+endl;
cout<<text;
string logfilename=Macrodirname+"/"+"log.csv";
ofstream logfile(logfilename, append); logfile<<text;



int[int] Order = [1, 1, 1, 1, 1];
string DataName = "TraceGrowth Growth1 Growth2 GrowthAni GrowthDir";
//savevtk(Macrodirname+"/"+"coupled_Growth_t"+string(t)+".vtk", ThTissue, tracegrowth, growth1, growth2, growthani, thetah, dataname=DataName, order=Order);

fesave(tracegrowth, Macrodirname+"/"+"coupled_TraceGrowth_t"+string(t)+".txt");


// Euler method for time evolution --> new Fg
// --------------------------------
Fg = summat(Fg,dt*matprod(G2,Fg));


// write the new growth tensor Fg
// -----------------------------
VhC FgC0=Fg[0];
VhC FgC1=Fg[1];
VhC FgC2=Fg[2];
VhC FgC3=Fg[3];


cout << "writing Fg tensor elements at t="<<tstamp1<<endl;

fesave(FgC0,Macrodirname+"/"+"Fg0_t"+tstamp1+".txt");
fesave(FgC1,Macrodirname+"/"+"Fg1_t"+tstamp1+".txt");
fesave(FgC2,Macrodirname+"/"+"Fg2_t"+tstamp1+".txt");
fesave(FgC3,Macrodirname+"/"+"Fg3_t"+tstamp1+".txt");

/*
VhC u1C=u[0]-translx, u2C=u[1]-transly;
fesave(u1C,Macrodirname+"/"+"u1_t"+tstamp+".txt");
fesave(u2C,Macrodirname+"/"+"u2_t"+tstamp+".txt");


VhC u1C=u[0], u2C=u[1];
fesave(u1C,Macrodirname+"/"+"u1_t"+string(t)+".txt");
fesave(u2C,Macrodirname+"/"+"u2_t"+string(t)+".txt");
*/

Sh0 u1P1=u1-translx;
Sh0 u2P1=u2-transly;

fesave(u1P1,Macrodirname+"/"+"u1_t"+string(t)+".txt");
fesave(u2P1,Macrodirname+"/"+"u2_t"+string(t)+".txt");
plot(u1P1, cmm="u1P1", ps=Macrodirname+"/"+"u1P1_t"+tstamp+".eps", bb=bbtissue, wait=false, value=true,fill=1, hsv=HSVviridis);
plot(u2P1, cmm="u2P1", ps=Macrodirname+"/"+"u2P1_t"+tstamp+".eps", bb=bbtissue, wait=false, value=true,fill=1, hsv=HSVviridis);

bool debug = true;
include "getARGV.idp"


int nx=getARGV("-nx", 25);
real coarseedge=getARGV("-coarse_edge", .5);
real L=getARGV("-L", 1.);
real H=getARGV("-H", 1./2.);

// cell dimensions
real l10 = getARGV("-l1", 1.);
real l20 = getARGV("-l2", 1.);
real l1pl20=l10/l20;
//real theta=getARGV("-theta", 30)*pi/180;
real wall0 = getARGV("-wall", 0.1);

// cellwall mechanical parameters
real Ecw = getARGV("-Ecw", 1.0);  // Ecw
real nu=getARGV("-nu", 0.3);  // nu

include "definitions/bib_general.cpp";
include "geometry/geometry_macroscopic.cpp";
include "visualisation/colormaps.cpp";
include "visualisation/boundingboxes_tissue.cpp";

string Macrodirname=getARGV("-macrodir", ".");

plot(ThTissue,ps=Macrodirname+"/ThTissue.eps", bb=bbtissue, wait=false);
plot(ThTissueCoarse,ps=Macrodirname+"/ThTissueCoarse.eps" , bb=bbtissue, wait=false);


// save coordinates of coarse mesh dof
// -------------------------------------
VhC X=x, Y=y;
fesave(X,Macrodirname+"/"+"X0.txt");
fesave(Y,Macrodirname+"/"+"X1.txt");


// initialise Fg growth tensor to the identity
// --------------------------------------------
VhC id=1, nul=0;
fesave(id,Macrodirname+"/"+"Fg0_t0.00.txt");
fesave(nul,Macrodirname+"/"+"Fg1_t0.00.txt");
fesave(nul,Macrodirname+"/"+"Fg2_t0.00.txt");
fesave(id,Macrodirname+"/"+"Fg3_t0.00.txt");


// geometrical parameter field on the coarse mesh
// --------------------------------
include "definitions/param_Fields_geometry.cpp";

VhC wallh=wallfunc;
plot(wallh, cmm="wallthickness",ps=Macrodirname+"/wallCoarse.eps" , bb=bbtissue, wait=false, fill=true, value=true, hsv=HSVviridis);
fesave(wallh,Macrodirname+"/"+"wall.txt");

VhC l2h=l2func;
plot(l2h, cmm="l2",ps=Macrodirname+"/l2Coarse.eps" , bb=bbtissue, wait=false, fill=true, value=true, hsv=HSVviridis);
fesave(l2h,Macrodirname+"/"+"l2.txt");

VhC l1pl2h=l1pl2func;
plot(l1pl2h, cmm="l1pl2",ps=Macrodirname+"/l1pl2Coarse.eps" , bb=bbtissue, wait=false, fill=true, value=true, hsv=HSVviridis);
fesave(l1pl2h,Macrodirname+"/"+"l1pl2.txt");


// Ecw as field on the coarse mesh
// --------------------------------
include "definitions/param_Fields_elasticity.cpp";

VhC Ecwh=Efunc;
plot(Ecwh, cmm="Ecw",ps=Macrodirname+"/EcwCoarse.eps" , bb=bbtissue, wait=false, fill=true, value=true, hsv=HSVviridis);
fesave(Ecwh,Macrodirname+"/"+"Ecw.txt");

VhC nuh=nufunc;
plot(nuh, cmm="nu",ps=Macrodirname+"/nuCoarse.eps" , bb=bbtissue, wait=false, fill=true, value=true, hsv=HSVviridis);
fesave(nuh,Macrodirname+"/"+"nu.txt");



// geometrical parameters
// -----------------

// cellwall thickness
real wallin=wall0, wallout=wall0;
func wallfunc=wallin + (wallout-wallin)/L*x;// gradient along Ox


// cell size
real l2in=l20, l2out=l20;
//real val=.2;
//real l2in=val, l2out=val;
func l2func=l2in + (l2out-l2in)/L*x;// gradient along Ox


// cell shape 
real l1pl2in=l1pl20, l1pl2out=l1pl20;
func l1pl2func=l1pl2in + (l1pl2out-l1pl2in)/L*x;// gradient along Ox




// --------------- 2D matrix computations ------------

// definition of matrices
macro id [1., 0., 0., 1.] //
macro nul [0., 0., 0., 0.] //

// matrix operations with matrices stored as vectors
macro printmat(A) 
{
cout<<"--------------"<<endl;
cout<<A[0]<<" "<<A[1]<<endl;
cout<<A[2]<<" "<<A[3]<<endl;
cout<<"--------------"<<endl;
} //

// trace
macro trace(A) (A[0]+A[3]) //

// determinant
macro det(A) (A[0]*A[3]-A[1]*A[2]) //

// matrix product
macro matprod(A, B) [ A[0]*B[0]+A[1]*B[2], A[0]*B[1]+A[1]*B[3],
                     A[2]*B[0]+A[3]*B[2], A[2]*B[1]+A[3]*B[3]  ] //

macro traceprod(A,B)  trace(matprod(A,B)) //

// sum and difference
macro summat(A, B) [ A[0]+B[0], A[1]+B[1], A[2]+B[2], A[3]+B[3] ] //
macro difmat(A, B) [ A[0]-B[0], A[1]-B[1], A[2]-B[2], A[3]-B[3] ] //

// inverse of a matrix
macro invert(A) ((1./(det(A)+.0))*[ A[3], -A[1], -A[2], A[0] ])//

// transpose
macro transp(A) [ A[0], A[2], A[1], A[3] ]//

// symmetric part
macro sym(A) (1./2.)*summat(A,transp(A))//

// tensor product with A and B supposed symmetric
macro tensprod(M,A,B)  (M[0]*A[0]*B[0]
					+M[1]*A[3]*B[3] 
					+M[2]*(A[0]*B[3]+A[3]*B[0]) 
					+4*M[3]*A[1]*B[1] 
					+2*M[4]*(A[0]*B[1]+A[1]*B[0]) 
					+2*M[5]*(A[3]*B[1]+A[1]*B[3])) //

// scalar product of two vectors
macro scalprod(U,V) (U[0]*V[0]+U[1]*V[1]) //

// product of a matrix and a vector
macro matvectprod(A,U) [A[0]*U[0]+A[1]*U[1], A[2]*U[0]+A[3]*U[1]] //

// product of a 4-order tensor to a matrix (2-order tensor) with right symmetries
macro tensmatprod(M,A)  [M[0]*A[0]+2*M[4]*A[1]+M[2]*A[3],
                         M[4]*A[0]+2*M[3]*A[1]+M[5]*A[3],
                         M[4]*A[0]+2*M[3]*A[1]+M[5]*A[3],
                         M[2]*A[0]+2*M[5]*A[1]+M[1]*A[3]] //

// norm of a 2D vector
macro norm(U) (sqrt(U[0]*U[0]+U[1]*U[1])) //

// rotation matrix
macro R(alpha) [cos(alpha), -sin(alpha), sin(alpha), cos(alpha)] //

// rotate a tensor
macro Rtensor(A, alpha)   matprod(matprod(R(alpha),A),transp(R(alpha))) //

// projection on the normal component of the boundary
macro pinu(u) [N.x*(u[0]*N.x+u[1]*N.y), N.y*(u[0]*N.x+u[1]*N.y)] //

// projection on the tangential component of the boundary
macro pitau(u) [u[0]-N.x*(u[0]*N.x+u[1]*N.y), u[1]-N.y*(u[0]*N.x+u[1]*N.y)] //

// ()+
macro absplus(X) (X+abs(X))/2. //


//---------- Model specific definitions --------

// elasticity tensor
macro Etensor(mu, lambda) [2*mu+lambda, 2*mu+lambda, lambda, mu, 0, 0]//


// deformation and growth tensor
macro Jg(Fg)  det(Fg) //
macro grad(u) [dx(u[0]),  dy(u[0]), dx(u[1]), dy(u[1])] // deformation gradient
macro graduFg1(u,Fg)    matprod(grad(u), invert(Fg))//
macro graduFg1sym(u,Fg) sym(graduFg1(u,Fg))//


// strain
macro strain1(u,Fg)     graduFg1sym(u,Fg) //
macro strain0(Fg)     difmat(sym(invert(Fg)),id) //
macro eps(u,Fg)   summat(strain1(u,Fg), strain0(Fg)) //

// stress
macro stress1(Em,u,Fg)  tensmatprod(Em,strain1(u,Fg)) //
macro stress0(Em,Fg)  tensmatprod(Em,strain0(Fg)) //
macro sigma(Em,u,Fg)      tensmatprod(Em,eps(u,Fg)) //

// elasticity
//macro elasticity1(Em,u,Fg,v)     Jg(Fg)*traceprod(stress1(Em,u,Fg),graduFg1(v,Fg))//
macro elasticity1(Em,u,Fg,v)     Jg(Fg)*tensprod(Em,graduFg1sym(u,Fg),graduFg1sym(v,Fg))//
macro elasticity0(Em,Fg,v)     Jg(Fg)*traceprod(stress0(Em,Fg),graduFg1(v,Fg))//

// pressurefield
macro pressurefield(K,P,Fg,v)   Jg(Fg)*P*traceprod(K,graduFg1(v,Fg))//
macro pressurefield0(P,Fg,v)   Jg(Fg)*P*trace(graduFg1(v,Fg))//

macro Fg1T(Fg)       transp(invert(Fg)) //
macro Fg1TgradP(Fg,P)  matvectprod(Fg1T(Fg),[dx(P), dy(P)]) //
macro gradpressurefield(P,Fg,v)   Jg(Fg)*scalprod(Fg1TgradP(Fg,P),v)//

// pressure on boundary
macro Fg1TN(Fg)      matvectprod(Fg1T(Fg),[N.x,N.y]) //
macro pressureonboundary(P,Fg,v)   Jg(Fg)*P*scalprod(Fg1TN(Fg),v) //
macro pressureonboundaryD(P,Fg,v)   Jg(Fg)*P*scalprod(pitau(Fg1TN(Fg)),pitau(v)) //
macro pressureonboundaryNu(P,Fg,v)   Jg(Fg)*P*scalprod(pinu(Fg1TN(Fg)),pinu(v)) //

// P2 pressure term
macro P2term(Fg, P2, v)   Jg(Fg)*scalprod(matvectprod(Fg1T(Fg),P2),v)  //

// external force
macro Fg1TNnorm(Fg)  norm(matvectprod(Fg1T(Fg),[N.x,N.y])) //
macro externalforce(f,Fg,v)       Jg(Fg)*Fg1TNnorm(Fg)*scalprod(f,v) //





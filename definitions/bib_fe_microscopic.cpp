/* This file contains FE field declarations
 * for the microscopic model*/


// ------ define the finite element space

fespace Sh0(ThTissue,P1);   // scalar on the mesh, P1 elements
fespace Vh2(ThTissue,[P1,P1]);  // 2D array on the mesh (u), P1 elements -- OUT
fespace Vh4(ThTissue,[P1,P1,P1,P1]);  // 4D array on the mesh (Fg, eps), P1 elements
fespace Vh6(ThTissue,[P1,P1,P1,P1,P1,P1]);  // 6D array on the mesh (Em), P1 elements


// FE fields in the variational formulation

macro u [u1, u2] //
Vh2 u;

macro v [v1, v2] //
Vh2 v;

macro E [Em1, Em2, Em3, Em4, Em5, Em6] //
Vh6 E;

macro Egt [Emgt1, Emgt2, Emgt3, Emgt4, Emgt5, Emgt6] //
Vh6 Egt;

macro Fg [Fg1, Fg2, Fg3, Fg4] //
Vh4 Fg;

macro G2 [G21, G22, G23, G24] //
Vh4 G2;



func perio=[[leftwalls,y],[rightwalls,y], [bottomwalls,x], [topwalls,x]]; // ============== OUT

// ------ define the finite element space

fespace Sh0(ThTissue,P2);   // scalar on the mesh, P0 elements
fespace Vh2(ThTissue,[P2,P2],periodic=perio);  // ============== OUT
fespace Vh4(ThTissue,[P1,P1,P1,P1]);  // vector on the mesh (Fg, eps), P1 elements
fespace Vh6(ThTissue,[P1,P1,P1,P1,P1,P1]);  // vector on the mesh (Em), P1 elements


// FE fields in the variational formulation

macro u [u1, u2] //
Vh2 u;

macro v [v1, v2] //
Vh2 v;

macro E [Em1, Em2, Em3, Em4, Em5, Em6] //
Vh6 E;

macro Fg [Fg1, Fg2, Fg3, Fg4] //
Vh4 Fg;


macro G2 [G21, G22, G23, G24] //
Vh4 G2;



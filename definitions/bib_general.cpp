/* This file contains macro definitions
 * that are common for all models */


macro fesave(fe, filename) {
	cout<<"Saving FE field into file "<<filename<<"..."<<endl;
	ofstream outfile(filename); outfile << fe[];
} //


macro feload(fe, filename) {
	cout<<"Loading FE field from file "<<filename<<"..."<<endl;
	{ifstream h(filename); h >> fe[];}
} //



func string decimalnum(real x, int dec)
{
	int integerpart=int(x);
	string nombre=string(integerpart);
	int i;
	if (dec>0)
	{
	nombre=nombre+".";
	for (i=1;i<=dec;i++){
		cout<<"i:"<<i<<"   "<<nombre<<"    "<<int((x*10^i-strtod(nombre)*10^i))<<endl;
		nombre=nombre+string(int((x*10^i-strtod(nombre)*10^i)));
		cout<<"i:"<<i<<"   "<<nombre<<"    "<<strtod(nombre)<<endl;
		
		cout<<"nombre="<<nombre<<endl;
		}
	}
	return nombre;
}



func string stringt(real X)
{
	return decimalnum(X,2);
}



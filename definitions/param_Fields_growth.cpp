

// extensibility
// --------------
real Cin=sigmaC, Cout=sigmaC;
func Cfunc=Cin + (Cout-Cin)/L*x;// sigmaC gradient along Ox


// stress threshold
// --------------
real Tin=sigmaThreshold, Tout=sigmaThreshold;  // 0.002
func Tfunc=Tin + (Tout-Tin)/L*x;// sigmaThreshold gradient along Ox


// ------ define the finite element space

fespace Sh0(ThTissue,P1);   // scalar on the mesh, P1 elements
fespace Vh2(ThTissue,[P2,P2]);  // ============== OUT
fespace Vh4(ThTissue,[P1,P1,P1,P1]);  // vector on the mesh (Fg, eps), P1 elements
fespace Vh6(ThTissue,[P1,P1,P1,P1,P1,P1]);  // vector on the mesh (Em), P1 elements


// FE fields in the variational formulation

macro u [u1, u2] //
Vh2 u;

macro v [v1, v2] //
Vh2 v;

macro E [Em1, Em2, Em3, Em4, Em5, Em6] //
Vh6 E;

macro Eaux [Eauxm1, Eauxm2, Eauxm3, Eauxm4, Eauxm5, Eauxm6] //
Vh6 Eaux;

macro Fg [Fg1, Fg2, Fg3, Fg4] //
Vh4 Fg;

macro G2 [G21, G22, G23, G24] //
Vh4 G2;

macro K [K21, K22, K23, K24] //
Vh4 K;

macro Kaux [Kaux21, Kaux22, Kaux23, Kaux24] //
Vh4 Kaux;

macro Mat [Mat1, Mat2, Mat3, Mat4] //
Vh4 Mat;

macro Tw11 [Tw11m1, Tw11m2, Tw11m3, Tw11m4] //
Vh4 Tw11;

macro Tw12 [Tw12m1, Tw12m2, Tw12m3, Tw12m4] //
Vh4 Tw12;

macro Tw22 [Tw22m1, Tw22m2, Tw223, Tw224] //
Vh4 Tw22;

macro Tw [Twm1, Twm2, Twm3, Twm4] //
Vh4 Tw;


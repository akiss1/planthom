

// elasticity tensor
// -----------------

// cell wall Young modulus Ecw
real Ein=Ecw, Eout=Ecw;
func Efunc=Ein + (Eout-Ein)/L*x;// gradient along Ox
//func Efunc=Ein + (Eout-Ein)*sin(x*pi/L);// gradient along Ox with a peak in the middle

// Poisson ratio nu
real nuin=nu, nuout=nu;
//real nuin=0., nuout=0.99;
func nufunc=nuin + (nuout-nuin)/L*x;// gradient along Ox


// pressure
// --------
real Pin=Pressure0, Pout=Pin*.1;

//func Pressure=Pin + (Pout-Pin)/L*x; // pressure gradient along Ox
//func Pressure=Pin+(Pout-Pin)*int((x>L/2.)) ;// pressure step function along Ox

func Pressure=Pin + (Pout-Pin)/L*((x-L/2)^2+y^2) ; // circular pressure gradient

from math import modf

# growthtype parameter encodes the hypothesis on growth :
# strain based growth : 0
# stress-based growth : 1

growthtype=0


def intpart(x):
	return modf(x)[1]

# =========  Parameters ====
Nxfact=2

# time
# -------

Ntpoints=11
tmin=0.
tmax=60.
dt=(tmax-tmin)/(Ntpoints-1)


# mesh for microscopic simulation
# -----------------------
nx0=25
nx=nx0*Nxfact

# meshes for coupled simulation
# ----------------------------
# macroscopic pb: fine mesh (edges/unit length)
nxmacro=10
# macroscopic pb: coarse mesh (edge length)
coarse_edge=1
# unit cell pb
nxUC=25

# macro geometry
# ---------------------------
Nx0=4
Nx=Nx0*Nxfact

# Ny0 supposed even
Ny0=2
Ny=Ny0*Nxfact


# micro geometry
# ---------------
l10=1.
l20=1.
theta=30
wall0=.05

l1=l10/Nxfact
l2=l20/Nxfact
wall=wall0/Nxfact


extwall=wall*0.25

# micro mechanical properties
# ---------------------------
Ecw=1.
nu=0.3
P0=0.001

sigmaC=1.
sigmaThreshold=0.

# macro parameters
# ---------------------------
thetarad=theta*pi/180
L=(Nx0*2)*l20*cos(thetarad) 
H=(Ny0-1/2.)*(l10+2*l20*sin(thetarad))



// bounding box for the plots 
// (use the same for all images so that they can be superposed)


//func bbtissue=[[-L*0.1,-H*0.1],[L*1.1,H*1.1]];
func bbtissue=[[-L*0.1,-H*0.1],[L*2,H*2]];
func bbzoom=[[L/2,H/2],[L/2+H/2*0.5,H/2*1.5]];

// bounding box for the plots 
// (use the same for all images so that they can be superposed)


func bbcell=[[-l2*cos(theta)*1.5,-(l1/2+l2*sin(theta))*1.5],[l2*cos(theta)*1.5,(l1/2+l2*sin(theta))*1.5]];

//coeficient of displacement amplification for plotting deformed tissue
real rho=1;

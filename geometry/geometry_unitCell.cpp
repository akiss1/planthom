// lineD and lineC functions give the coefficients 
// in the equation y=D*x+C of a line passing through the points A and B
func real lineD(real xA, real xB, real yA, real yB)
{	return (yB-yA)/(xB-xA);
}

func real lineC(real xA, real xB, real yA, real yB)
{	return (yA*xB-yB*xA)/(xB-xA);
}

func real angleToVertical(real Mx, real My, real Nx, real Ny)
{ /*  Computes the sinus of angle between the MN vector and Oy   */
	return (Nx-Mx)/sqrt((Mx-Nx)^2+(My-Ny)^2);
}

func real Striangle( real a0, real a1, real b0, real b1)
{
	return abs(a0*b1-a1*b0)/2.;
}

func real length(real Mx, real My, real Nx, real Ny)
{
	return sqrt((Mx-Nx)^2+(My-Ny)^2);
}


/*************************************************************
 *                  GEOMETRY and MESH
 * **********************************************************/
int Ncells=7; // number of cells in the elementary structure 
int Next=10, Nint=100000;

int leftwalls=207;
int bottomwalls=3407;
int rightwalls=507;
int topwalls=6107;
int fixedwalls=707;

// ----------------- external boundary of one cell


// whole cell
border a1(t=0,1){x=t*(-l2*cos(theta));y=l1/2+(1-t)*l2*sin(theta);label=1;};
border a2(t=0,1){x=-l2*cos(theta);y=l1/2-t*l1;label=2;};
border a3(t=0,1){x=-l2*cos(theta)*(1-t);y=-l1/2-t*l2*sin(theta);label=3;};
border a4(t=0,1){x=t*l2*cos(theta);y=-l1/2-(1-t)*l2*sin(theta);label=4;};
border a5(t=0,1){x=l2*cos(theta);y=-l1/2+t*l1;label=5;};
border a6(t=0,1){x=(1-t)*l2*cos(theta);y=l1/2+t*l2*sin(theta);label=6;};

border a2left(t=0,1){x=-l2*cos(theta);y=l1/2-t*l1;label=leftwalls;};
border a5right(t=0,1){x=l2*cos(theta);y=-l1/2+t*l1;label=rightwalls;};

// cell cut on the "down" part
border da2(t=0,1){x=-l2*cos(theta);y=l1/2-t*l1/2;label=2;};
border da3(t=0,1){x=-l2*cos(theta)+t*hwall;y=0;label=bottomwalls;};
border da4(t=1,0){x=l2*cos(theta)-t*hwall;y=0;label=bottomwalls;};
border da5(t=0,1){x=l2*cos(theta);y=0+t*l1/2;label=5;};

border da2left(t=0,1){x=-l2*cos(theta);y=l1/2-t*l1/2;label=leftwalls;};
border da5right(t=0,1){x=l2*cos(theta);y=0+t*l1/2;label=rightwalls;};


// cell cut on the "up" part
border ua1(t=0,1){x=-l2*cos(theta)+hwall-t*hwall;y=0;label=topwalls;};
border ua2(t=0,1){x=-l2*cos(theta);y=0-t*l1/2;label=2;};
border ua5(t=0,1){x=l2*cos(theta);y=-l1/2+t*l1/2;label=5;};
border ua6(t=0,1){x=l2*cos(theta)-t*hwall;y=0;label=topwalls;};

border ua2left(t=0,1){x=-l2*cos(theta);y=0-t*l1/2;label=leftwalls;};
border ua5right(t=0,1){x=l2*cos(theta);y=-l1/2+t*l1/2;label=rightwalls;};

// cell cut on the "left" part
border la21(t=0,1){x=0;y=l1/2+l2*sin(theta)-t*hwall/cos(theta);label=leftwalls;};
border la22(t=0,1){x=0;y=-l1/2-l2*sin(theta)+hwall/cos(theta)*(1-t);label=leftwalls;};

// cell cut on the "right" part
border ra51(t=1,0){x=0;y=l1/2+l2*sin(theta)-t*hwall/cos(theta);label=rightwalls;};
border ra52(t=1,0){x=0;y=-l1/2-l2*sin(theta)+hwall/cos(theta)*(1-t);label=rightwalls;};


// ----------------- internal boundary of one cell

real xM=0, xN=l2*cos(theta)-hwall, 
     yM=l1/2+l2*sin(theta)-hwall/cos(theta), yN=l1/2+hwall*(sin(theta)-1)/cos(theta);
cout<<"xN="<<xN<<"  yN="<<yN<<endl;


// whole cell
border b1(t=0,1){x=xM+t*(-xN-xM);y=yM+t*(yN-yM);label=Nint+1;};
border b2(t=0,1){x=-xN;y=yN-t*(2*yN);label=Nint+2;};
border b3(t=0,1){x=-xN+t*(xN);y=-yN+t*(-yM+yN);label=Nint+3;};
border b4(t=0,1){x=xM+t*(xN-xM);y=-yM+t*(-yN+yM);label=Nint+4;};
border b5(t=0,1){x=xN;y=-yN+t*(2*yN);label=Nint+5;};
border b6(t=0,1){x=xN+t*(xM-xN);y=yN+t*(yM-yN);label=Nint+6;};


// cell cut on the "down" part
border db1(t=1,0){x=xM+t*(-xN-xM);y=yM+t*(yN-yM);label=Nint+1;};
border db2(t=1,0){x=-xN;y=yN-t*(yN);label=Nint+2;};
border db5(t=1,0){x=xN;y=t*(yN);label=Nint+5;};
border db6(t=1,0){x=xN+t*(xM-xN);y=yN+t*(yM-yN);label=Nint+6;};

// cell cut on the "up" part
border ub2(t=1,0){x=-xN;y=0-t*(yN);label=Nint+2;};
border ub3(t=1,0){x=-xN+t*(xN);y=-yN+t*(-yM+yN);label=Nint+3;};
border ub4(t=1,0){x=xM+t*(xN-xM);y=-yM+t*(-yN+yM);label=Nint+4;};
border ub5(t=1,0){x=xN;y=-yN+t*(yN);label=Nint+5;};

// cell cut on the "left" part
border lb4(t=1,0){x=xM+t*(xN-xM);y=-yM+t*(-yN+yM);label=Nint+4;};
border lb5(t=1,0){x=xN;y=-yN+t*(2*yN);label=Nint+5;};
border lb6(t=1,0){x=xN+t*(xM-xN);y=yN+t*(yM-yN);label=Nint+6;};

// cell cut on the "right" part
border rb1(t=1,0){x=xM+t*(-xN-xM);y=yM+t*(yN-yM);label=Nint+1;};
border rb2(t=1,0){x=-xN;y=yN-t*(2*yN);label=Nint+2;};
border rb3(t=1,0){x=-xN+t*(xN);y=-yN+t*(-yM+yN);label=Nint+3;};



/*
plot(a1(nvertex*l2)+a2(nvertex*l1)+a3(nvertex*l2)
      + a4(nvertex*l2)+a5(nvertex*l1)+a6(nvertex*l2)
      + b1(-nvertex*l2)+b2(-nvertex*l1)+b3(-nvertex*l2)
      + b4(-nvertex*l2)+b5(-nvertex*l1)+b6(-nvertex*l2)
    , wait=1);
*/

// ------------------- define the mesh for one cell
mesh ThCell = buildmesh (a1(nvertex*l2)+a2(nvertex*l1)+a3(nvertex*l2)
                     + a4(nvertex*l2)+a5(nvertex*l1)+a6(nvertex*l2)
                     + b1(-nvertex*l2)+b2(-nvertex*l1)+b3(-nvertex*l2)
					 + b4(-nvertex*l2)+b5(-nvertex*l1)+b6(-nvertex*l2)
					 , fixedborder=true);
//plot(ThCell,wait=1);

cout<<"ThCellLeftCutBottom..."<<endl;
mesh ThCellLeftCutBottom = buildmesh (a1(nvertex*l2)+da2left(nvertex*l1/2)+da3(nvertex*hwall)+da4(nvertex*hwall)
                     +da5(nvertex*l1/2)+a6(nvertex*l2)
                     + db1(nvertex*l2)+db2(nvertex*l1/2)+db5(nvertex*l1/2)+db6(nvertex*l2)
                    , fixedborder=true);



cout<<"ThCellRightCutBottom..."<<endl;
mesh ThCellRightCutBottom = buildmesh (a1(nvertex*l2)+da2(nvertex*l1/2)+da3(nvertex*hwall)+da4(nvertex*hwall)
                     +da5right(nvertex*l1/2)+a6(nvertex*l2)
                     + db1(nvertex*l2)+db2(nvertex*l1/2)+db5(nvertex*l1/2)+db6(nvertex*l2)
                    , fixedborder=true);

cout<<"ThCellLeftCutTop..."<<endl;
mesh ThCellLeftCutTop = buildmesh (ua1(nvertex*hwall)+ua2left(nvertex*l1/2)+a3(nvertex*l2)
      + a4(nvertex*l2)+ua5(nvertex*l1/2)+ua6(nvertex*hwall)
      + ub3(nvertex*l2)+ub2(nvertex*l1/2)+ub5(nvertex*l1/2)+ub4(nvertex*l2)
                    , fixedborder=true);

cout<<"ThCellRightCutTop..."<<endl;
mesh ThCellRightCutTop = buildmesh (ua1(nvertex*hwall)+ua2(nvertex*l1/2)+a3(nvertex*l2)
      + a4(nvertex*l2)+ua5right(nvertex*l1/2)+ua6(nvertex*hwall)
      + ub3(nvertex*l2)+ub2(nvertex*l1/2)+ub5(nvertex*l1/2)+ub4(nvertex*l2)
                    , fixedborder=true);

cout<<"ThCellCutLeft..."<<endl;
mesh ThCellCutLeft = buildmesh (la21(nvertex*hwall)+la22(nvertex*hwall)
                     + a4(nvertex*l2)+a5(nvertex*l1)+a6(nvertex*l2)
					 + lb4(nvertex*l2)+lb5(nvertex*l1)+lb6(nvertex*l2)
                    , fixedborder=true);

cout<<"ThCellCutRight..."<<endl;
mesh ThCellCutRight = buildmesh (a1(nvertex*l2)+a2(nvertex*l1)+a3(nvertex*l2)
                     + ra51(nvertex*hwall)+ra52(nvertex*hwall)
                     + rb1(nvertex*l2)+rb2(nvertex*l1)+rb3(nvertex*l2)
                    , fixedborder=true);



// ------------------- construct the tissue mesh

real xdist=2*l2*cos(theta),
     ydist=2*(l1/2)+l2*sin(theta);

real[int] CX(Ncells), CY(Ncells); // positions of the cell centers

//int[int,int] cn(Nx,Ny);  // labels of cells
int[int,int] ExtLabel(Ncells,6), NewExtLabel(Ncells,6);  // labels of external border segments of cells
int[int,int] IntLabel(Ncells,6), NewIntLabel(Ncells,6);;  // labels of internal border segments of cells

int lglue=0, internal=1000; // label for glued border segments

int i,j,k, c0;
int[int] reg, lab(12), labint(12); // label vectors for region and border label changes

int cellindex;
int[int] icell(7), jcell(7);

icell(0)=0; jcell(0)=0;
icell(1)=1; jcell(1)=0;

icell(2)=0; jcell(2)=1;
icell(3)=1; jcell(3)=1;
icell(4)=2; jcell(4)=1;

icell(5)=0; jcell(5)=2;
icell(6)=1; jcell(6)=2;

// compute cell attributes (label, center, border labels)
for (cellindex=1; cellindex<=7; cellindex++)
{
	i=icell(cellindex-1);
	j=jcell(cellindex-1);
	c0=cellindex;
	
	//
	ExtLabel(c0-1,0)=c0*Next+1;  ExtLabel(c0-1,1)=c0*Next+2;  ExtLabel(c0-1,2)=c0*Next+3;
	ExtLabel(c0-1,3)=c0*Next+4;  ExtLabel(c0-1,4)=c0*Next+5;  ExtLabel(c0-1,5)=c0*Next+6;
	//
	IntLabel(c0-1,0)=c0*Nint+1;  IntLabel(c0-1,1)=c0*Nint+2;  IntLabel(c0-1,2)=c0*Nint+3;
	IntLabel(c0-1,3)=c0*Nint+4;  IntLabel(c0-1,4)=c0*Nint+5;  IntLabel(c0-1,5)=c0*Nint+6;
	//
	CX(c0-1)=i*xdist-(j%2)*xdist/2;     CY(c0-1)=0.+j*ydist;
}

NewExtLabel=ExtLabel;
NewIntLabel=IntLabel;

// compute border labels after glueing
for (c0=1; c0<=7; c0++)
{
	for (k=0; k<6; k++){NewExtLabel(c0-1,k)=lglue; } 
	for (k=0; k<6; k++){NewIntLabel(c0-1,k)=internal; } cout<<endl;
}

// Construct the tissue

// ThTissue: tissue mesh
// Thm: moved cell mesh before gluing (an intermediate construction)
mesh ThTissue, Thm; 

// Construct the tissue 
// ---------------------

for (c0=1; c0<=Ncells; c0++)
{
	i=icell(c0-1);
	j=jcell(c0-1);
	//cout<<"Cell number "<<c0<<endl;
	reg=[0, c0];
	for (k=0; k<6; k++){lab[2*k]=k+1;
						lab[2*k+1]=NewExtLabel(c0-1,k);} 
	for (k=0; k<6; k++){labint[2*k]=Nint+k+1;
						labint[2*k+1]=NewIntLabel(c0-1,k);}
	//for (k=0; k<6; k++){cout << "  " <<ExtLabel(c0-1,k); } cout<<endl;	
	if (c0==1) { // first cell
		Thm=movemesh(ThCellLeftCutBottom, [x+CX(c0-1), y+CY(c0-1)]);
		Thm=change(Thm, label=lab);
		Thm=change(Thm, label=labint);
		ThTissue=Thm;}
	else if (c0==2 ) { // first line except first  cell
		Thm=movemesh(ThCellRightCutBottom, [x+CX(c0-1), y+CY(c0-1)]);
		Thm=change(Thm, label=lab);
		Thm=change(Thm, label=labint);
		ThTissue=ThTissue+Thm;}
	else if (c0==3) { // first column except first cell - paratlan sor
		Thm=movemesh(ThCellCutLeft, [x+CX(c0-1), y+CY(c0-1)]);
		Thm=change(Thm, label=lab);
		Thm=change(Thm, label=labint);
		ThTissue=ThTissue+Thm;}
	else if (c0==5) { // last column - paros sor
		Thm=movemesh(ThCellCutRight, [x+CX(c0-1), y+CY(c0-1)]);
		Thm=change(Thm, label=lab);
		Thm=change(Thm, label=labint);
		ThTissue=ThTissue+Thm;}
	else if (c0==6)  { // first cell of the last line
		Thm=movemesh(ThCellLeftCutTop, [x+CX(c0-1), y+CY(c0-1)]);
		Thm=change(Thm, label=lab);
		Thm=change(Thm, label=labint);
		ThTissue=ThTissue+Thm;}
	else if (c0==7)  { // last line except its first cell
		Thm=movemesh(ThCellRightCutTop, [x+CX(c0-1), y+CY(c0-1)]);
		Thm=change(Thm, label=lab);
		Thm=change(Thm, label=labint);
		ThTissue=ThTissue+Thm;}
	else // c0=4
		{
		Thm=movemesh(ThCell, [x+CX(c0-1), y+CY(c0-1)]);
		Thm=change(Thm, label=lab);
		Thm=change(Thm, label=labint);
		ThTissue=ThTissue+Thm;};	
	//for (k=0; k<6; k++){cout << "  " <<NewExtLabel(c0-1,k); } cout<<endl;
	ThTissue=change(ThTissue, region=reg);
	//plot(ThCell, wait=1);
	//plot(ThTissue, wait=1);
}


// move the mesh so that the center of the unit cell is at the origin
ThTissue=movemesh(ThTissue, [x-CX(3), y-CY(3)]);


/*************************************************************
 *                  DEFINITION OF DOMAINS
 * **********************************************************/


/*

// cell wall and middle lamella are separated
real xxxM=0, xxxN=l2*cos(theta)-ml, 
     yyyM=l1/2+l2*sin(theta)-ml/cos(theta), yyyN=l1/2+ml*(sin(theta)-1)/cos(theta);
//cout<<"xxN="<<xN<<"  yyN="<<yN<<endl;



func bool wallBool(real xx, real yy, int i)
{	
	real D1, D3, D4, D6, C1, C3, C4, C6;
	D1=lineD(xxxM+CX[i], -xxxN+CX[i], yyyM+CY[i], yyyN+CY[i]); C1=lineC(xxxM+CX[i], -xxxN+CX[i], yyyM+CY[i], yyyN+CY[i]);
	D3=lineD(-xxxN+CX[i], xxxM+CX[i], -yyyN+CY[i], -yyyM+CY[i]); C3=lineC(-xxxN+CX[i], xxxM+CX[i], -yyyN+CY[i], -yyyM+CY[i]);
	D4=lineD(xxxM+CX[i], xxxN+CX[i], -yyyM+CY[i], -yyyN+CY[i]); C4=lineC(xxxM+CX[i], xxxN+CX[i], -yyyM+CY[i], -yyyN+CY[i]);
	D6=lineD(xxxN+CX[i], xxxM+CX[i], yyyN+CY[i], yyyM+CY[i]); C6=lineC(xxxN+CX[i], xxxM+CX[i], yyyN+CY[i], yyyM+CY[i]);
	return ((yy<=D1*xx+C1) && (xx>=-xxxN+CX[i]) && (yy>=D3*xx+C3) && (yy>=D4*xx+C4) && ( xx<=xxxN+CX[i]) && (yy<=D6*xx+C6) ) ;
}


// cell wall and middle lamella together
real xxM=0, xxN=l2*cos(theta), 
yyM=l1/2+l2*sin(theta), yyN=l1/2;
//cout<<"xxN="<<xN<<"  yyN="<<yN<<endl;


func bool wallBoolt(real xx, real yy, int i)
{	
	real D1, D3, D4, D6, C1, C3, C4, C6;
	D1=lineD(xxM+CX[i], -xxN+CX[i], yyM+CY[i], yyN+CY[i]); C1=lineC(xxM+CX[i], -xxN+CX[i], yyM+CY[i], yyN+CY[i]);
	D3=lineD(-xxN+CX[i], xxM+CX[i], -yyN+CY[i], -yyM+CY[i]); C3=lineC(-xxN+CX[i], xxM+CX[i], -yyN+CY[i], -yyM+CY[i]);
	D4=lineD(xxM+CX[i], xxN+CX[i], -yyM+CY[i], -yyN+CY[i]); C4=lineC(xxM+CX[i], xxN+CX[i], -yyM+CY[i], -yyN+CY[i]);
	D6=lineD(xxN+CX[i], xxM+CX[i], yyN+CY[i], yyM+CY[i]); C6=lineC(xxN+CX[i], xxM+CX[i], yyN+CY[i], yyM+CY[i]);
	return ((yy<=D1*xx+C1) && (xx>=-xxN+CX[i]) && (yy>=D3*xx+C3) && (yy>=D4*xx+C4) && ( xx<=xxN+CX[i]) && (yy<=D6*xx+C6) ) ;
}


// ===== wall-direction functions === theta1 --- theta6

func int theta1(real xx, real yy, int i)
{	
	real Da1, Ca1;
	Da1=lineD(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]); Ca1=lineC(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]);
	return int(((yy>=Da1*xx+Ca1) && (xx<=CX[i]) )*wallBoolt(xx,yy,i)) ;
}


func int theta2(real xx, real yy, int i)
{	
	real Da1, Ca1, Da2, Ca2;
	Da1=lineD(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]); Ca1=lineC(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]);
	Da2=lineD(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]); Ca2=lineC(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]);
	return int(((yy<=Da1*xx+Ca1) && (yy>=Da2*xx+Ca2) )*wallBoolt(xx,yy,i)) ;
}


func int theta3(real xx, real yy, int i)
{	
	real Da2, Ca2;
	Da2=lineD(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]); Ca2=lineC(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]);
	return int(((xx<=CX[i]) && (yy<=Da2*xx+Ca2) )*wallBoolt(xx,yy,i)) ;
}


func int theta4(real xx, real yy, int i)
{	
	real Da1, Ca1;
	Da1=lineD(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]); Ca1=lineC(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]);
	return int(((yy<=Da1*xx+Ca1) && (xx>=CX[i]) )*wallBoolt(xx,yy,i)) ;
}


func int theta5(real xx, real yy, int i)
{	
	real Da1, Ca1, Da2, Ca2;
	Da1=lineD(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]); Ca1=lineC(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]);
	Da2=lineD(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]); Ca2=lineC(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]);
	return int(((yy>=Da1*xx+Ca1) && (yy<=Da2*xx+Ca2) )*wallBoolt(xx,yy,i)) ;
}


func int theta6(real xx, real yy, int i)
{	
	real Da2, Ca2;
	Da2=lineD(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]); Ca2=lineC(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]);
	return int(((xx>=CX[i]) && (yy>=Da2*xx+Ca2) )*wallBoolt(xx,yy,i)) ;
}


// ===================================================================

func real DistmapCell(real xx, real yy, int i)
{
	real D1, C1, D3, C3, D4, C4, D6, C6;
	D1=lineD(xxM+CX[i], -xxN+CX[i], yyM+CY[i], yyN+CY[i]); C1=lineC(xxM+CX[i], -xxN+CX[i], yyM+CY[i], yyN+CY[i]);
	D3=lineD(-xxN+CX[i], xxM+CX[i], -yyN+CY[i], -yyM+CY[i]); C3=lineC(-xxN+CX[i], xxM+CX[i], -yyN+CY[i], -yyM+CY[i]);
	D4=lineD(xxM+CX[i], xxN+CX[i], -yyM+CY[i], -yyN+CY[i]); C4=lineC(xxM+CX[i], xxN+CX[i], -yyM+CY[i], -yyN+CY[i]);
	D6=lineD(xxN+CX[i], xxM+CX[i], yyN+CY[i], yyM+CY[i]); C6=lineC(xxN+CX[i], xxM+CX[i], yyN+CY[i], yyM+CY[i]);
	return  ( abs(D1*xx-yy+C1)/sqrt(D1*D1+1)*theta1(xx,yy,i)
	+(xx-CX[i]+xxN)*theta2(xx,yy,i)
	+abs(D3*xx-yy+C3)/sqrt(D3*D3+1)*theta3(xx,yy,i)
	+abs(D4*xx-yy+C4)/sqrt(D4*D4+1)*theta4(xx,yy,i)
	+(-xx+CX[i]+xxN)*theta5(xx,yy,i)
	+abs(D6*xx-yy+C6)/sqrt(D6*D6+1)*theta6(xx,yy,i) ) ;
}

*/

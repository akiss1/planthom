// lineD and lineC functions give the coefficients 
// in the equation y=D*x+C of a line passing through the points A and B
func real lineD(real xA, real xB, real yA, real yB)
{	return (yB-yA)/(xB-xA);
}

func real lineC(real xA, real xB, real yA, real yB)
{	return (yA*xB-yB*xA)/(xB-xA);
}

func real angleToVertical(real Mx, real My, real Nx, real Ny)
{ /*  Computes the sinus of angle between the MN vector and Oy   */
	return (Nx-Mx)/sqrt((Mx-Nx)^2+(My-Ny)^2);
}

/*************************************************************
 *                  GEOMETRY and MESH
 * **********************************************************/
int Next=10, Nint=100000, Nskin=2000;

int skinlabel=Nskin;

int leftwalls=207;
int bottomwalls=307;
int rightwalls=507;
int topwalls=607;

int outer=707;

int lglue=0, internal=1000; // label for glued border segments
int leftborder=27; // label of the very left cellborders on which BC can be put at u1=0
int rightborder=57; // label of the very left cellborders on which BC can be put at u2=0



// ----------------- external boundary of one cell


// whole cell
border a1(t=0,1){x=t*(-l2*cos(theta));y=l1/2+(1-t)*l2*sin(theta);label=1;};
border a2(t=0,1){x=-l2*cos(theta);y=l1/2-t*l1;label=2;};
border a3(t=0,1){x=-l2*cos(theta)*(1-t);y=-l1/2-t*l2*sin(theta);label=3;};
border a4(t=0,1){x=t*l2*cos(theta);y=-l1/2-(1-t)*l2*sin(theta);label=4;};
border a5(t=0,1){x=l2*cos(theta);y=-l1/2+t*l1;label=5;};
border a6(t=0,1){x=(1-t)*l2*cos(theta);y=l1/2+t*l2*sin(theta);label=6;};

border a2left(t=0,1){x=-l2*cos(theta);y=l1/2-t*l1;label=leftwalls;};
border a5right(t=0,1){x=l2*cos(theta);y=-l1/2+t*l1;label=rightwalls;};
border a5ext(t=0,1){x=l2*cos(theta);y=-l1/2+t*l1;label=lglue;};
border a6ext(t=0,1){x=(1-t)*l2*cos(theta);y=l1/2+t*l2*sin(theta);label=lglue;};

// cell cut on the "down" part
border da2(t=0,1){x=-l2*cos(theta);y=l1/2-t*l1/2;label=2;};
border da3(t=0,1){x=-l2*cos(theta)+t*hwall;y=0;label=bottomwalls;};
border da4(t=1,0){x=l2*cos(theta)-t*hwall;y=0;label=bottomwalls;};
border da5(t=0,1){x=l2*cos(theta);y=0+t*l1/2;label=5;};

border da2left(t=0,1){x=-l2*cos(theta);y=l1/2-t*l1/2;label=leftwalls;};

// cell cut on the "left" part
border la21(t=0,1){x=0;y=l1/2+l2*sin(theta)-t*hwall/cos(theta);label=leftwalls;};
border la22(t=0,1){x=0;y=-l1/2-l2*sin(theta)+hwall/cos(theta)*(1-t);label=leftwalls;};


// ----------------- internal boundary of one cell

real xM=0, xN=l2*cos(theta)-hwall, 
     yM=l1/2+l2*sin(theta)-hwall/cos(theta), yN=l1/2+hwall*(sin(theta)-1)/cos(theta);
cout<<"xN="<<xN<<"  yN="<<yN<<endl;


// whole cell
border b1(t=0,1){x=xM+t*(-xN-xM);y=yM+t*(yN-yM);label=Nint+1;};
border b2(t=0,1){x=-xN;y=yN-t*(2*yN);label=Nint+2;};
border b3(t=0,1){x=-xN+t*(xN);y=-yN+t*(-yM+yN);label=Nint+3;};
border b4(t=0,1){x=xM+t*(xN-xM);y=-yM+t*(-yN+yM);label=Nint+4;};
border b5(t=0,1){x=xN;y=-yN+t*(2*yN);label=Nint+5;};
border b6(t=0,1){x=xN+t*(xM-xN);y=yN+t*(yM-yN);label=Nint+6;};


// cell cut on the "down" part
border db1(t=1,0){x=xM+t*(-xN-xM);y=yM+t*(yN-yM);label=Nint+1;};
border db2(t=1,0){x=-xN;y=yN-t*(yN);label=Nint+2;};
border db5(t=1,0){x=xN;y=t*(yN);label=Nint+5;};
border db6(t=1,0){x=xN+t*(xM-xN);y=yN+t*(yM-yN);label=Nint+6;};


// cell cut on the "left" part
border lb4(t=1,0){x=xM+t*(xN-xM);y=-yM+t*(-yN+yM);label=Nint+4;};
border lb5(t=1,0){x=xN;y=-yN+t*(2*yN);label=Nint+5;};
border lb6(t=1,0){x=xN+t*(xM-xN);y=yN+t*(yM-yN);label=Nint+6;};

// ------------------- define the mesh for one cell
mesh ThCell = buildmesh (a1(nvertex*l2)+a2(nvertex*l1)+a3(nvertex*l2)
                     + a4(nvertex*l2)+a5(nvertex*l1)+a6(nvertex*l2)
                     + b1(-nvertex*l2)+b2(-nvertex*l1)+b3(-nvertex*l2)
					 + b4(-nvertex*l2)+b5(-nvertex*l1)+b6(-nvertex*l2), fixedborder=1
                    );


mesh ThCellCutBottom = buildmesh (a1(nvertex*l2)+da2(nvertex*l1/2)+da3(nvertex*hwall)+da4(nvertex*hwall)
                     +da5(nvertex*l1/2)+a6(nvertex*l2)
                     + db1(nvertex*l2)+db2(nvertex*l1/2)+db5(nvertex*l1/2)+db6(nvertex*l2), fixedborder=1
                    );


mesh ThCellCutLeft = buildmesh (la21(nvertex*hwall)+la22(nvertex*hwall)
                     + a4(nvertex*l2)+a5(nvertex*l1)+a6(nvertex*l2)
					 + lb4(nvertex*l2)+lb5(nvertex*l1)+lb6(nvertex*l2), fixedborder=1
                    );



mesh ThCellLeft = buildmesh (a1(nvertex*l2)+a2left(nvertex*l1)+a3(nvertex*l2)
                     + a4(nvertex*l2)+a5(nvertex*l1)+a6(nvertex*l2)
                     + b1(-nvertex*l2)+b2(-nvertex*l1)+b3(-nvertex*l2)
					 + b4(-nvertex*l2)+b5(-nvertex*l1)+b6(-nvertex*l2), fixedborder=1
                    );

mesh ThCell0 = buildmesh (a1(nvertex*l2)+da2left(nvertex*l1/2)+da3(nvertex*hwall)+da4(nvertex*hwall)
                     +da5(nvertex*l1/2)+a6(nvertex*l2)
                     + db1(nvertex*l2)+db2(nvertex*l1/2)+db5(nvertex*l1/2)+db6(nvertex*l2), fixedborder=1
                    );


mesh ThCellext = buildmesh (a1(nvertex*l2)+a2(nvertex*l1)+a3(nvertex*l2)
                     + a4(nvertex*l2)+a5ext(nvertex*l1)+a6ext(nvertex*l2)
                     + b1(-nvertex*l2)+b2(-nvertex*l1)+b3(-nvertex*l2)
					 + b4(-nvertex*l2)+b5(-nvertex*l1)+b6(-nvertex*l2), fixedborder=1
                    );



// ------------------- construct the tissue mesh

real xdist=2*l2*cos(theta),
     ydist=2*(l1/2)+l2*sin(theta);

int Ncells=Nx*Ny;

real[int] CX(Ncells), CY(Ncells); // positions of the cell centers



int[int,int] cn(Nx,Ny);  // labels of cells
int[int,int] ExtLabel(Ncells,6), NewExtLabel(Ncells,6);  // labels of external border segments of cells
int[int,int] IntLabel(Ncells,6), NewIntLabel(Ncells,6);;  // labels of internal border segments of cells



int i,j,k, c0;
int[int] reg, lab(12), labint(12); // label vectors for region and border label changes

int cellcount=0;
// compute cell attributes (label, center, border labels)
for (j=0; j<=Ny-1; j++)
{
 for (i=0; i<=Nx-1; i++)
 {
	cout<<j<<","<<j<<endl;
	cellcount=cellcount+1;
	cn(i,j)=cellcount;
	c0=cn(i,j);
	//
	ExtLabel(c0-1,0)=c0*Next+1;  ExtLabel(c0-1,1)=c0*Next+2;  ExtLabel(c0-1,2)=c0*Next+3;
	ExtLabel(c0-1,3)=c0*Next+4;  ExtLabel(c0-1,4)=c0*Next+5;  ExtLabel(c0-1,5)=c0*Next+6;
	//
	IntLabel(c0-1,0)=c0*Nint+1;  IntLabel(c0-1,1)=c0*Nint+2;  IntLabel(c0-1,2)=c0*Nint+3;
	IntLabel(c0-1,3)=c0*Nint+4;  IntLabel(c0-1,4)=c0*Nint+5;  IntLabel(c0-1,5)=c0*Nint+6;
	//
	CX(c0-1)=i*xdist-(j%2)*xdist/2 + xdist/2;     CY(c0-1)=0.+j*ydist;
}
}

NewExtLabel=ExtLabel;
NewIntLabel=IntLabel;

// compute border labels after glueing (put all external cellwalls at label lglue)
for (j=0; j<=Ny-1; j++)
{
 for (i=0; i<=Nx-1; i++)
 {
	c0=cn(i,j);
	NewExtLabel(c0-1,5)=lglue;  // border 6
	NewExtLabel(c0-1,0)=lglue;  // border 1
	NewExtLabel(c0-1,4)=lglue;  // border 5
	NewExtLabel(c0-1,1)=lglue;  // border 2	
	NewExtLabel(c0-1,3)=lglue;  // border 4
	NewExtLabel(c0-1,2)=lglue;  // border 3
	for (k=0; k<6; k++){NewIntLabel(c0-1,k)=internal; } cout<<endl;
}
}


// Construct the tissue

// ThTissue: tissue mesh
// Thm: moved cell mesh before gluing (an intermediate construction)
mesh ThTissue, Thm; 


// Construct the tissue 
// ---------------------

int skinindex=skinlabel;

for (j=0; j<=Ny-1; j++)
{
	 for (i=0; i<=Nx-1; i++)
	{
	c0=cn(i,j);
	cout<<"Cell number "<<c0<<endl;
	reg=[0, c0];
	if (c0==1) { // first cell
		Thm=movemesh(ThCell0, [x+CX(c0-1), y+CY(c0-1)]);
		for (k=0; k<6; k++){lab[2*k]=k+1;
						lab[2*k+1]=NewExtLabel(c0-1,k);} 
		for (k=0; k<6; k++){labint[2*k]=Nint+k+1;
						labint[2*k+1]=NewIntLabel(c0-1,k);} 
		for (k=0; k<6; k++){cout << "  " <<ExtLabel(c0-1,k); } cout<<endl;
		for (k=0; k<6; k++){cout << "  " <<int1d(Thm,ExtLabel(c0-1,k))(1.); } cout<<endl;
			Thm=change(Thm, label=lab);
			Thm=change(Thm, label=labint);
		for (k=0; k<6; k++){cout << "  " <<NewExtLabel(c0-1,k); } cout<<endl;
		for (k=0; k<6; k++){cout << "  " <<int1d(Thm,NewExtLabel(c0-1,k))(1.); } cout<<endl;
			ThTissue=Thm;
			ThTissue=change(ThTissue, region=reg);
			}
	else if (j==0) { // first line except first cell
		Thm=movemesh(ThCellCutBottom, [x+CX(c0-1), y+CY(c0-1)]);
		for (k=0; k<6; k++){lab[2*k]=k+1;
						lab[2*k+1]=NewExtLabel(c0-1,k);} 
		for (k=0; k<6; k++){labint[2*k]=Nint+k+1;
						labint[2*k+1]=NewIntLabel(c0-1,k);} 
		for (k=0; k<6; k++){cout << "  " <<ExtLabel(c0-1,k); } cout<<endl;
		for (k=0; k<6; k++){cout << "  " <<int1d(Thm,ExtLabel(c0-1,k))(1.); } cout<<endl;
			Thm=change(Thm, label=lab);
			Thm=change(Thm, label=labint);
		for (k=0; k<6; k++){cout << "  " <<NewExtLabel(c0-1,k); } cout<<endl;
		for (k=0; k<6; k++){cout << "  " <<int1d(Thm,NewExtLabel(c0-1,k))(1.); } cout<<endl;
			ThTissue=ThTissue+Thm;
			ThTissue=change(ThTissue, region=reg);}
	else if (i==0 && j%2==0)  { // first column except first cell - paros sor
		Thm=movemesh(ThCellLeft, [x+CX(c0-1), y+CY(c0-1)]);
		for (k=0; k<6; k++){lab[2*k]=k+1;
						lab[2*k+1]=NewExtLabel(c0-1,k);} 
		for (k=0; k<6; k++){labint[2*k]=Nint+k+1;
						labint[2*k+1]=NewIntLabel(c0-1,k);}
		for (k=0; k<6; k++){cout << "  " <<ExtLabel(c0-1,k); } cout<<endl;
		for (k=0; k<6; k++){cout << "  " <<int1d(Thm,ExtLabel(c0-1,k))(1.); } cout<<endl;
			Thm=change(Thm, label=lab);
			Thm=change(Thm, label=labint);
		for (k=0; k<6; k++){cout << "  " <<NewExtLabel(c0-1,k); } cout<<endl;
		for (k=0; k<6; k++){cout << "  " <<int1d(Thm,NewExtLabel(c0-1,k))(1.); } cout<<endl;
			ThTissue=ThTissue+Thm;
			ThTissue=change(ThTissue, region=reg);
			}
	else if (i==0 && j%2==1)  { // first column except first cell - paratlan sor
		Thm=movemesh(ThCellLeft, [x+CX(c0-1), y+CY(c0-1)]); ////////////////////////////////////
		for (k=0; k<6; k++){lab[2*k]=k+1;
						lab[2*k+1]=NewExtLabel(c0-1,k);} 
		for (k=0; k<6; k++){labint[2*k]=Nint+k+1;
						labint[2*k+1]=NewIntLabel(c0-1,k);}
		for (k=0; k<6; k++){cout << "  " <<ExtLabel(c0-1,k); } cout<<endl;
		for (k=0; k<6; k++){cout << "  " <<int1d(Thm,ExtLabel(c0-1,k))(1.); } cout<<endl;
			Thm=change(Thm, label=lab);
			Thm=change(Thm, label=labint);
		for (k=0; k<6; k++){cout << "  " <<NewExtLabel(c0-1,k); } cout<<endl;
		for (k=0; k<6; k++){cout << "  " <<int1d(Thm,NewExtLabel(c0-1,k))(1.); } cout<<endl;
			ThTissue=ThTissue+Thm;
			ThTissue=change(ThTissue, region=reg);}
	else 
		{
		Thm=movemesh(ThCell, [x+CX(c0-1), y+CY(c0-1)]);
		for (k=0; k<6; k++){lab[2*k]=k+1;
						lab[2*k+1]=NewExtLabel(c0-1,k);} 
		for (k=0; k<6; k++){labint[2*k]=Nint+k+1;
						labint[2*k+1]=NewIntLabel(c0-1,k);}
		for (k=0; k<6; k++){cout << "  " <<ExtLabel(c0-1,k); } cout<<endl;
		for (k=0; k<6; k++){cout << "  " <<int1d(Thm,ExtLabel(c0-1,k))(1.); } cout<<endl;
			Thm=change(Thm, label=lab);
			Thm=change(Thm, label=labint);
		for (k=0; k<6; k++){cout << "  " <<NewExtLabel(c0-1,k); } cout<<endl;
		for (k=0; k<6; k++){cout << "  " <<int1d(Thm,NewExtLabel(c0-1,k))(1.); } cout<<endl;
			ThTissue=ThTissue+Thm;
			ThTissue=change(ThTissue, region=reg);};
	//ThTissue=change(ThTissue, region=reg);
	//plot(ThCell, wait=1);
	//plot(ThTissue, wait=1);
}


}


//plot(ThTissue, wait=1);


// Print for check
cout<<"Initial and final cellborder labels, as well as checking:"<<endl;
cout<<"========================================================="<<endl;
for (j=0; j<=Ny-1; j++)
{
	 for (i=0; i<=(Nx-1); i++)
	{
	c0=cn(i,j);
	cout<<"Cell number "<<c0<<endl;
	cout<<"Region number "<< ThTissue(CX[c0-1]-l2*cos(theta)+0.001,CY[c0-1]).region <<endl;
	//
	cout << "ExtLabel:"; for (k=0; k<6; k++){cout << "  " <<ExtLabel(c0-1,k); } cout<<endl;
	cout << "NewExtLabel:"; for (k=0; k<6; k++){cout << "  " <<NewExtLabel(c0-1,k); } cout<<endl;
	cout << "Length of NewExtLabel in the whole tissue:"; for (k=0; k<6; k++){cout << "  " <<int1d(ThTissue,NewExtLabel(c0-1,k))(1.); } cout<<endl;
}
}


// moving so that the first left wall is on the Oy axis
//ThTissue = movemesh(ThTissue, [x+l2*cos(theta), y]);


/*************************************************************
 *                  DEFINITION OF DOMAINS
 * **********************************************************/


// cell wall and middle lamella are separated
real xxxM=0, xxxN=l2*cos(theta)-ml, 
     yyyM=l1/2+l2*sin(theta)-ml/cos(theta), yyyN=l1/2+ml*(sin(theta)-1)/cos(theta);
cout<<"xxN="<<xN<<"  yyN="<<yN<<endl;



func bool wallBool(real xx, real yy, int i)
{	
	real D1, D3, D4, D6, C1, C3, C4, C6;
	D1=lineD(xxxM+CX[i], -xxxN+CX[i], yyyM+CY[i], yyyN+CY[i]); C1=lineC(xxxM+CX[i], -xxxN+CX[i], yyyM+CY[i], yyyN+CY[i]);
	D3=lineD(-xxxN+CX[i], xxxM+CX[i], -yyyN+CY[i], -yyyM+CY[i]); C3=lineC(-xxxN+CX[i], xxxM+CX[i], -yyyN+CY[i], -yyyM+CY[i]);
	D4=lineD(xxxM+CX[i], xxxN+CX[i], -yyyM+CY[i], -yyyN+CY[i]); C4=lineC(xxxM+CX[i], xxxN+CX[i], -yyyM+CY[i], -yyyN+CY[i]);
	D6=lineD(xxxN+CX[i], xxxM+CX[i], yyyN+CY[i], yyyM+CY[i]); C6=lineC(xxxN+CX[i], xxxM+CX[i], yyyN+CY[i], yyyM+CY[i]);
	return ((yy<=D1*xx+C1) && (xx>=-xxxN+CX[i]) && (yy>=D3*xx+C3) && (yy>=D4*xx+C4) && ( xx<=xxxN+CX[i]) && (yy<=D6*xx+C6) ) ;
}



// cell wall and middle lamella together
real xxM=0, xxN=l2*cos(theta), 
yyM=l1/2+l2*sin(theta), yyN=l1/2;
cout<<"xxN="<<xN<<"  yyN="<<yN<<endl;



func bool wallBoolt(real xx, real yy, int i)
{	
	real D1, D3, D4, D6, C1, C3, C4, C6;
	D1=lineD(xxM+CX[i], -xxN+CX[i], yyM+CY[i], yyN+CY[i]); C1=lineC(xxM+CX[i], -xxN+CX[i], yyM+CY[i], yyN+CY[i]);
	D3=lineD(-xxN+CX[i], xxM+CX[i], -yyN+CY[i], -yyM+CY[i]); C3=lineC(-xxN+CX[i], xxM+CX[i], -yyN+CY[i], -yyM+CY[i]);
	D4=lineD(xxM+CX[i], xxN+CX[i], -yyM+CY[i], -yyN+CY[i]); C4=lineC(xxM+CX[i], xxN+CX[i], -yyM+CY[i], -yyN+CY[i]);
	D6=lineD(xxN+CX[i], xxM+CX[i], yyN+CY[i], yyM+CY[i]); C6=lineC(xxN+CX[i], xxM+CX[i], yyN+CY[i], yyM+CY[i]);
	return ((yy<=D1*xx+C1) && (xx>=-xxN+CX[i]) && (yy>=D3*xx+C3) && (yy>=D4*xx+C4) && ( xx<=xxN+CX[i]) && (yy<=D6*xx+C6) ) ;
}


// ===== wall-direction functions === theta1 --- theta6

func int theta1(real xx, real yy, int i)
{	
	real Da1, Ca1;
	Da1=lineD(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]); Ca1=lineC(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]);
	return int(((yy>=Da1*xx+Ca1) && (xx<=CX[i]) )*wallBoolt(xx,yy,i)) ;
}


func int theta2(real xx, real yy, int i)
{	
	real Da1, Ca1, Da2, Ca2;
	Da1=lineD(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]); Ca1=lineC(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]);
	Da2=lineD(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]); Ca2=lineC(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]);
	return int(((yy<=Da1*xx+Ca1) && (yy>=Da2*xx+Ca2) )*wallBoolt(xx,yy,i)) ;
}


func int theta3(real xx, real yy, int i)
{	
	real Da2, Ca2;
	Da2=lineD(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]); Ca2=lineC(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]);
	return int(((xx<=CX[i]) && (yy<=Da2*xx+Ca2) )*wallBoolt(xx,yy,i)) ;
}


func int theta4(real xx, real yy, int i)
{	
	real Da1, Ca1;
	Da1=lineD(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]); Ca1=lineC(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]);
	return int(((yy<=Da1*xx+Ca1) && (xx>=CX[i]) )*wallBoolt(xx,yy,i)) ;
}


func int theta5(real xx, real yy, int i)
{	
	real Da1, Ca1, Da2, Ca2;
	Da1=lineD(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]); Ca1=lineC(-xxN+CX[i], CX[i], yyN+CY[i], CY[i]);
	Da2=lineD(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]); Ca2=lineC(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]);
	return int(((yy>=Da1*xx+Ca1) && (yy<=Da2*xx+Ca2) )*wallBoolt(xx,yy,i)) ;
}


func int theta6(real xx, real yy, int i)
{	
	real Da2, Ca2;
	Da2=lineD(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]); Ca2=lineC(-xxN+CX[i], CX[i], -yyN+CY[i], CY[i]);
	return int(((xx>=CX[i]) && (yy>=Da2*xx+Ca2) )*wallBoolt(xx,yy,i)) ;
}




// ===================================================================

func real DistmapCell(real xx, real yy, int i)
{
	real D1, C1, D3, C3, D4, C4, D6, C6;
	D1=lineD(xxM+CX[i], -xxN+CX[i], yyM+CY[i], yyN+CY[i]); C1=lineC(xxM+CX[i], -xxN+CX[i], yyM+CY[i], yyN+CY[i]);
	D3=lineD(-xxN+CX[i], xxM+CX[i], -yyN+CY[i], -yyM+CY[i]); C3=lineC(-xxN+CX[i], xxM+CX[i], -yyN+CY[i], -yyM+CY[i]);
	D4=lineD(xxM+CX[i], xxN+CX[i], -yyM+CY[i], -yyN+CY[i]); C4=lineC(xxM+CX[i], xxN+CX[i], -yyM+CY[i], -yyN+CY[i]);
	D6=lineD(xxN+CX[i], xxM+CX[i], yyN+CY[i], yyM+CY[i]); C6=lineC(xxN+CX[i], xxM+CX[i], yyN+CY[i], yyM+CY[i]);
	return  ( abs(D1*xx-yy+C1)/sqrt(D1*D1+1)*theta1(xx,yy,i)+(xx-CX[i]+xxN)*theta2(xx,yy,i)+abs(D3*xx-yy+C3)/sqrt(D3*D3+1)*theta3(xx,yy,i)
	+abs(D4*xx-yy+C4)/sqrt(D4*D4+1)*theta4(xx,yy,i)+(-xx+CX[i]+xxN)*theta5(xx,yy,i)+abs(D6*xx-yy+C6)/sqrt(D6*D6+1)*theta6(xx,yy,i) ) ;
}

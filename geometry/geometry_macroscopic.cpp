
// mesh parameters
cout << "nx="<<nx<<endl;

/*************************************************************
 *                  GEOMETRY and MESH
 * **********************************************************/

// Fine mesh


int[int] labs0=[10,20,30,40]; // bottom, right, top, left
mesh Th0=square(L*nx,H*nx, label=labs0, region=0, [x*L,y*H]);

int[int] labs1=[110,120,130,140]; // bottom, right, top, left
mesh Th1=square(L*nx,H*nx, label=labs1, region=0, [x*L,-y*H]);

int[int] labsT=[10,20,40]; // bottom, diagonal, left
border a1(t=0,1){x=t*2*L;y=0;label=labsT[0];};
border a2(t=0,1){x=2*L*(1-t);y=2*H*t;label=labsT[1];};
border a3(t=0,1){x=0;y=2*H*(1-t);label=labsT[2];};
real diag=2*sqrt(L*L+H*H);
mesh ThT = buildmesh (a1(nx*2*L)+a2(nx*diag)+a3(nx*2*H));

//mesh ThTissue=Th0+Th1;
mesh ThTissue=Th0;
//mesh ThTissue=ThT;


// Coarse mesh
mesh ThTissueCoarse=adaptmesh(ThTissue, 1., hmax=coarseedge, hmin=coarseedge);
fespace VhC(ThTissueCoarse,P1);


cout << "eof macroscopic_geometry.cpp"<<endl;
